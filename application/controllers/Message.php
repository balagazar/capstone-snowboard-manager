<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();
    // Your own constructor code
     $this->TPL['loggedin'] = $this->userauth->loggedin();
	$this->TPL['current_logged_username'] = $this->userauth->getUsername();
	$this->TPL['user_access'] = $this->userauth->getAccesslevel();
    $this->TPL['active'] = array('home' => false,
                                'rent'=>false,
                                'admin' => true,
                                'login'=>false,
								'products'=>false,
								'new_user'=>false,
								'contacts'=>false);
														
    $this->TPL['error'] = false;
	$this->TPL['reported_successfully'] = false;
    $this->TPL['userExists'] = false;
	$this->TPL['UserDoesNotExist'] = false;
	$this->TPL['CreatedUser_section_1'] = false;
	$this->TPL['message_view'] =false;
	$this->TPL['read_message'] = false;
	
  }
  public function index()
  {
	$this->load->library('form_validation');
    $this->display();
    
  }

    public function display()
	{
		
		$username=$this->userauth->getUsername();//Get username info
		$query = $this->db->query("Select* FROM message where message_to = '$username' order by message_id desc");
		$this->TPL['listing'] = $query->result_array();

	
		$this->template->show('message', $this->TPL);
	}
	
	 public function Send_Message($To = NULL,$subject = NULL)
	{
		$this->TPL['To'] =$To;
		if(isset($subject)){
			$this->TPL['subject'] ="RE:".str_replace("%20"," ",$subject);
		}
		$this->TPL['message_view'] =true;
		$this->template->show('message', $this->TPL);
	}
	
	public function Send_Message_process()
	{
		$To=$this->input->post("To");
		
		$query = $this->db->query("Select * from users where '$To' = username");
		
		if($query->num_rows()==0){
			$this->TPL['UserDoesNotExist'] = true;
			$this->TPL['message_view'] =true;
			$this->display();
        }else{
		
			$from=$this->userauth->getUsername();
			$subject=$this->input->post("Subject");
			$content=$this->input->post("Message_Content");
			$date = date("Y-m-d");
			$query = $this->db->query("INSERT INTO message(message_from,message_to,message_date,was_message_read,message_content,message_subject) VALUES ('$from', '$To','$date','N', '$content','$subject');");
		
			$this->display();
		}
	}
	
	public function delete_message()
	{
		$message_id =  $this->uri->segment(3);
		$query = $this->db->query("delete from message where message_id ='$message_id'");
		$this->display();
	}
	
	public function read_message()
	{
		$message_id =  $this->uri->segment(3);
		 $this->TPL['read_message'] = true;
		 $query = $this->db->query("update message set was_message_read = 'Y' where message_id ='$message_id'");
		 $query = $this->db->query("select * from message where message_id = '$message_id'");
		 $this->TPL['message'] = $query->result_array()[0];
		 $this->template->show('message', $this->TPL);
	}
	
	 public function Add_report()
	{
		$reported_by=$this->userauth->getUsername();
		$offender =  $this->uri->segment(3);
		$report_content =  $this->uri->segment(4);
		
		$date = date("Y-m-d");
		$query = $this->db->query("INSERT INTO reports(offender,reported_by,report_content,reported_date) VALUES ('$offender', '$reported_by','$report_content','$date');");
		
		$this->TPL['reported_successfully'] = true;
		
		$this->display();
	}
	
}
?>