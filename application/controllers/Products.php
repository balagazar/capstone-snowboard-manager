<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();
    // Your own constructor code
    $this->TPL['loggedin'] = $this->userauth->validSessionExists();
	$this->TPL['current_logged_username'] = $this->userauth->getUsername();
	$this->TPL['user_access'] = $this->userauth->getAccesslevel();
    
														
    $this->TPL['error'] = false;
    $this->TPL['userExists'] = false;
	$this->TPL['Single_Item'] = false;
	$this->TPL['no_reviews']= false;
	$this->TPL['add_product_view']= false;
	

	
  }
  public function index()
  {
	$this->load->library('form_validation');
    $this->display();
    
  }
  
  public function get_user_id()
  {
		$username = $this->userauth->getUsername();
		$sql = "SELECT user_id FROM users where username ='$username'";
		$query = $this->db->query($sql);
		$row = $query->result_array()[0];
		$user_id = $row['user_id'];
		return $user_id;
  }
  
    public function find_product($string = null){
    if($string != null && strlen($string) > 0){
      $sql = "select * from products where INSTR(products.product_name,'$string')";
      $query = $this->db->query($sql);
      $table = $query->result_array();
      echo json_encode($table);
    }else{
      $sql = "SELECT * FROM products";
      $query = $this->db->query($sql);
      $table = $query->result_array();
      echo json_encode($table);
    }
    
  }
  public function AddToCart($product_id = null)
  {
	  
	  $isloggedin = $this->userauth->loggedin();
	if($isloggedin){
		$current_logged_username = $this->userauth->getUsername();
        $query = $this->db->query("SELECT * FROM shopping_cart WHERE username = '$current_logged_username' and product_id = '$product_id'");
        if($query->num_rows()>0){
			$row = $query->result_array()[0];
			$current_quantity = $row['quantity'];
			$current_quantity +=1;
			$this->db->query("UPDATE shopping_cart SET quantity = '$current_quantity' WHERE username = '$username' and product_id = '$product_id' ");
        }else{
			$user_id=$this->get_user_id();
			$this->db->query("INSERT INTO shopping_cart(product_id,user_id,username,total,quantity) VALUES ('$product_id','$user_id', '$current_logged_username',0.0,'1');");
        }
		$this->display(); 
	}else{
		$this->template->show('login', $this->TPL);
	}
  }
  
  public function view_snowboards()
  {
	  $sql = "SELECT * FROM products where category = 'snowboards'";
		$query = $this->db->query($sql);
		$this->TPL['products'] = $query->result_array();
		
		$this->template->show('products', $this->TPL);
  }
  
  public function view_helmets()
  {
	  $sql = "SELECT * FROM products where category = 'helmet'";
		$query = $this->db->query($sql);
		$this->TPL['products'] = $query->result_array();
		
		$this->template->show('products', $this->TPL);
  }
  
  
   public function view_gloves()
  {
	  $sql = "SELECT * FROM products where category = 'gloves'";
		$query = $this->db->query($sql);
		$this->TPL['products'] = $query->result_array();
		
		$this->template->show('products', $this->TPL);
  }
   public function view_boots()
  {
	  $sql = "SELECT * FROM products where category = 'boots'";
		$query = $this->db->query($sql);
		$this->TPL['products'] = $query->result_array();
		
		$this->template->show('products', $this->TPL);
  }
  public function high_to_low_price()
  {
	  $sql = "Select * From products ORDER BY CAST(product_price AS DECIMAL(10,2)) DESC";
		$query = $this->db->query($sql);
		$this->TPL['products'] = $query->result_array();
		
		$this->template->show('products', $this->TPL);
  }
  public function low_to_high_price()
  {
	  $sql = "Select * From products ORDER BY CAST(product_price AS DECIMAL(10,2)) ASC";
		$query = $this->db->query($sql);
		$this->TPL['products'] = $query->result_array();
		
		$this->template->show('products', $this->TPL);
  }
  public function Single_Product($product_id = null)
  {
	  $this->TPL['Single_Item'] = true;
		$sql = "SELECT * FROM products where product_id = '$product_id'";
		$query = $this->db->query($sql);
		$this->TPL['product'] = $query->result_array()[0];
		
		$sql = "SELECT count(*) as count from product_reviews where product_id ='$product_id'";
		$query = $this->db->query($sql);
		$count = $query->result_array()['0'];
		$count = $count['count'];
		if($count > 0){
			$sql = "SELECT * FROM product_reviews where product_id = '$product_id' ORDER BY review_date DESC";
			$query = $this->db->query($sql);
			$product_reviews = $query->result_array();
			$this->TPL['product_reviews'] = $query->result_array();
			$total_review = 0;
			
			$rating = array(1 => 0,2 => 0,3 => 0,4 => 0,5 => 0);
			foreach($product_reviews as $review)
			{
				$total_review ++;
				$given_rating += $review['rating'];
				switch ($review['rating']){
				case 1:
					$rating['1'] ++;
					break;
				case 2:
					$rating['2'] ++;
					break;
				case 3:
					$rating['3'] ++;
					break;
				case 4:
					$rating['4'] ++;
					break;
				case 5:
					$rating['5'] ++;
					break;
				}
			}
			if($total_review>0){
				$this->TPL['average_rating']= $given_rating/$total_review;
				$this->TPL['rnd_average_rating']= round($given_rating/$total_review);
			}
			$this->TPL['total_reviws']= $total_review;
			$this->TPL['star_amounts']= $rating;
			
		  $this->template->show('products', $this->TPL);
		}else{
			$this->TPL['no_reviews']= true;
			$this->template->show('products', $this->TPL);
		}
  }
  
	  public function Insert_review()
	  {
		  $this->load->helper('url');
		  
		  $product_id = $this->input->post("product_id");
		  $rating = $this->input->post("rating");
		  $comment = $this->input->post("comment");
		  $username = $this->userauth->getUsername();
		  
			$sql = "INSERT INTO product_reviews (product_id,username,rating,review_date,review_content) VALUES ('$product_id','$username','$rating',NOW(),'$comment')";
			$query = $this->db->query($sql);
		   header("Location: /ci/index.php?/Products/Single_Product/".$product_id);
	  }
  

    public function display()
	{
		$sql = "select product_picture,product_name,category,product_price,product_id as product,ROUND((select avg(ABS(product_reviews.rating)) from product_reviews
       	where product_reviews.product_id = product)) as avg_rate from products";
		$query = $this->db->query($sql);
		$this->TPL['products'] = $query->result_array();
		
		$this->template->show('products', $this->TPL);
	}
	
	public function Add_product_view()
	{
		$this->TPL['add_product_view']= true;
		$this->display(); 
	}
	public function Add_product()
	{
		        $product_name = $this->input->post("product_name");
				$product_description = $this->input->post("product_description");
				$product_category = $this->input->post("category");
				$product_price = $this->input->post("price");
				
				
				$this->load->helper('file');
				$username=$this->userauth->getUsername();
				
				$path = "./application/assets/img/products";
				
				if(!is_dir($path)) //create the folder if it's not already exists
				{
				  mkdir($path,0755,TRUE);
				}
				$path = "./application/assets/img/products";
				
				$files = scandir($path, 1);
				
				$max_pic_num = 0;
				foreach($files as $file)
				{
					 if(preg_match_all('/\d+/', $file, $numbers))
					 {
						$lastnum = end($numbers[0]);
					 }
					if($lastnum > $max_pic_num)
					{
						$max_pic_num = $lastnum;
					}
				}
				
				$max_pic_num= $max_pic_num +1;
				
				$config['upload_path']          = './application/assets/img/products/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 10000;
                $config['max_width']            = 1500;
                $config['max_height']           = 1500;
				$config['file_name']           = 'pic'.$max_pic_num.'.jpg';
				
				$product_path = './application/assets/img/products/pic'.$max_pic_num.'.jpg';

                $this->load->library('upload', $config);
				$this->upload->initialize($config);
				
		
                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $this->TPL['uploaded_successfully']="The picture was uploaded successfully";
                }
				
				$this->upload->initialize($config);
			
			
			$query = $this->db->query("INSERT INTO products(category,product_price,product_description,product_name,product_picture) VALUES ('$product_category', '$product_price', '$product_description', '$product_name','$product_path');");
			
			header("Location: /ci/index.php?/Products");
	}
	
	public function delete_product($product_id = null)
	{
		$query = $this->db->query("DELETE FROM products WHERE product_id = $product_id");
		$this->display(); 
	}
	
}
?>