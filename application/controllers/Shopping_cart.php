<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping_cart extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();
    // Your own constructor code
     $this->TPL['loggedin'] = $this->userauth->loggedin();
	$this->TPL['current_logged_username'] = $this->userauth->getUsername();
	$this->TPL['user_access'] = $this->userauth->getAccesslevel();
    $this->TPL['active'] = array('home' => false,
                                'rent'=>false,
                                'admin' => true,
                                'login'=>false,
								'products'=>false,
								'new_user'=>false,
								'contacts'=>false);
														
    $this->TPL['error'] = false;
	$this->TPL['product_cart_empty'] = false;
	

	
  }
  public function index()
  {
	$this->load->library('form_validation');
    $this->display();
    
  }
  

    public function display()
	{
		$username = $this->userauth->getUsername();
		$sql = "SELECT shopping_cart.product_id,shopping_cart.username, products.product_picture, products.product_name,products.product_price,(shopping_cart.quantity * products.product_price) AS total,shopping_cart.quantity FROM shopping_cart  INNER JOIN products ON shopping_cart.product_id=products.product_id where username = '$username'";
		$query = $this->db->query($sql);
		$this->TPL['shopping_cart'] = $query->result_array();
		if($query->num_rows()==0){
			$this->TPL['product_cart_empty'] = true;
		}
		$sql = "SELECT (shopping_cart.quantity * products.product_price) AS total FROM shopping_cart  INNER JOIN products ON shopping_cart.product_id=products.product_id where username = '$username'";
		$query = $this->db->query($sql);
		$total_off_all = $query->result_array();
		
		foreach($total_off_all as $price)
		{
			$Total_Amount += $price['total'];
		}
		$this->TPL['totalAmount'] = $Total_Amount;
		
		$this->template->show('shopping_cart', $this->TPL);
	}
	
	public function change_total(){
		$quantity =  $this->uri->segment(3);
		$product_id =  $this->uri->segment(4);
	$current_logged_username = $this->userauth->getUsername();
		if($quantity != null){
      $sql = "UPDATE shopping_cart SET quantity = '$quantity' where username = '$current_logged_username' and product_id = '$product_id'";
	  $this->db->query($sql);
		$this->display();
		}
	}
	
	public function delete_item($product_id = null){
		$username = $this->userauth->getUsername();
		$sql = "Delete from shopping_cart where username = '$username' and product_id = '$product_id'";
		$this->db->query($sql);
		$this->display();
	}
	

	public function Add_To_Orders(){
		$shipping_line = str_replace("%20"," ",$this->uri->segment(3));//second
		$shipping_city =  str_replace("%20"," ",$this->uri->segment(4));//second
		$shipping_state =  str_replace("%20"," ",$this->uri->segment(5));//third
		$shipping_postal_code =  str_replace("%20"," ", $this->uri->segment(6));//fourth
		$shipping_country_code =  str_replace("%20"," ", $this->uri->segment(7));//fourth
		
		
		$username = $this->userauth->getUsername();
		$sql = "SELECT shopping_cart.product_id,shopping_cart.username, products.product_picture, products.product_name,products.product_price,(shopping_cart.quantity * products.product_price) AS total,shopping_cart.quantity FROM shopping_cart  INNER JOIN products ON shopping_cart.product_id=products.product_id where username = '$username'";
		$query = $this->db->query($sql);
		$shopping_cart_products = $query->result_array();
		
		foreach($shopping_cart_products as $product)
		{
			$product_id = $product['product_id'];
			$quantity = $product['quantity'];
			$sql = "SELECT * from orders where username = '$username' and product_id = '$product_id' ";
			$query = $this->db->query($sql);
			if($query->num_rows()>0){
				$selected_items=$query->result_array()[0];
				$quantity_of_items =  $selected_items['quantity'];
				$quantity = $quantity_of_items+1;
				$sql = "UPDATE orders set quantity = '$quantity_of_items' where username = '$username' and product_id = '$product_id' ";
			}else{
			$sql = "INSERT INTO orders (username,shipping_address,city,province,country,postal_code,product_id,quantity,status) VALUES ('$username','$shipping_line','$shipping_city','$shipping_state','$shipping_country_code','$shipping_postal_code','$product_id','$quantity','shipped')";
			$this->db->query($sql);
			}
		}
		
		$sql = "Delete from shopping_cart where username = '$username'";
		$this->db->query($sql);
		$this->TPL['product_cart_empty'] = true;
		$this->template->show('shopping_cart', $this->TPL);
	}
	
}
?>