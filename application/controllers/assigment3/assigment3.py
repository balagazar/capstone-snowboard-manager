import numpy as np
import xml.etree.ElementTree as et      
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

ps = PorterStemmer()

def read_reuters(path="Reuters21578", limit=21578):
    ''' Reads the Reuters-21578 corpus from the given path. (This is assumed to 
    be the cleaned-up version of the corpus provided for this code.) The limit 
    parameter can be used to stop reading after a certain number of documents 
    have been read.'''
    
    def get_dtags(it, index):
        '''Helper function to parse the <D> elements'''
        dtags = []
        while it[index+1].tag == "D":
            dtags.append(it[index+1].text)
            index += 1
        return dtags, index

    docs = []
    numdocs = 0
    
    for i in range(22):
        pad = ""
        if i<10:
            pad = "0"
        print "Reading",path+'\\reut2-0'+pad+str(i)+'.sgm'
        
        tree = et.parse(path+'\\reut2-0'+pad+str(i)+'.sgm')
        root = tree.getroot()
        
        it = tree.getiterator()
        
        
        index = 0
        while index < len(it):
            if it[index].tag == "REUTERS":
                if numdocs == limit:
                    return docs
                docs.append({})
                numdocs+=1
            elif it[index].tag.lower() in ["topics", "places","people","orgs","exchanges","companies"]:
                docs[numdocs-1][it[index].tag.lower()], index = get_dtags(it, index)
            elif numdocs > 0:
                docs[numdocs-1][it[index].tag.lower()] = it[index].text
            
            index +=1
   
    return docs


def textParse(bigString):    
    """input is big string, output is word list"""
    import re
    listOfTokens = re.split(r'\W*', bigString)
    return [tok.lower() for tok in listOfTokens if len(tok) > 2] 
    
def createVocabList(dataSet):
    """ dataSet is a list of word lists. Returns the set of words in the dataSet
    as a list."""
    vocabSet = set([])  #create empty set
    for document in dataSet:
        vocabSet = vocabSet.union(set(document)) #union of the two sets
    return list(vocabSet)
    
def bagOfWords2Vec(vocabList, inputList):
    """ vocabList is a set of words (as a list). inputList is a list of words
    occurring in a document. Returns a vector of integers indicating how many
    times each word in the vocabList occurs in the inputList"""
    d = {}
    for word in inputList:
        d[word] = d.get(word,0)+1
    returnVec = []
    for word in vocabList:
        returnVec.append(d.get(word,0))
    
    return returnVec
    
def setOfWords2Vec(vocabList, inputList):
    """ vocabList is a set of words (as a list). inputList is a list of words
    occurring in a document. Returns a vector of 1's and 0's to indicate
    the presence or absence of each word in vocabList"""
    d = {}
    for word in inputList:
        d[word] = 1
    returnVec = []
    for word in vocabList:
        returnVec.append(d.get(word,0))
    return returnVec
    

    
    
    
def trainNB0(trainMatrix,trainCategory):
    """given a list of word lists representing the documents in the training 
    set (trainMatrix) and a related list of categories for each document 
    (trainCategory), returns a probability estimate list ("theory") for each 
    class as well as the probability estimate for a document being a member of
    class 1.
    
    Uses "Bag of Words" model.
    
    Uses Laplace smoothing.
    
    Note that this only works for 2 classes with class labels 0 and 1.
    """
    numTrainDocs = len(trainMatrix)
    numWords = len(trainMatrix[0])
    pAbusive = sum(trainCategory)/float(numTrainDocs)
    p0Num = np.zeros(numWords); p1Num = np.zeros(numWords)       
    p0Denom = 0; p1Denom = 0                        
    for i in range(numTrainDocs):
        if trainCategory[i] == 1:
            p1Num += trainMatrix[i]
            p1Denom += sum(trainMatrix[i]) # denominator is the number of words in class 1
        else:
            p0Num += trainMatrix[i]
            p0Denom += sum(trainMatrix[i]) # denominator is the number of words in class 0
    p1Vect = (p1Num+1)/(p1Denom+numWords)        
    p0Vect = (p0Num+1)/(p0Denom+numWords)
    return p0Vect,p1Vect,pAbusive
    
    
def classifyNB(vec2Classify, p0Vec, p1Vec, pClass1):
    """vec2Classify is a vector of 1's and 0's (a set of words), the other 
    parameters represent the "theory" of class 0 and 1, and the probability
    estimate for class1. 
    
    Uses log probabilities.
    
    Note that this function is exactly the same whether we are using a set
    or bag of words model.
    
    Note that we are assuming only 2 classes."""    
    p1 = sum(vec2Classify * np.log(p1Vec)) + np.log(pClass1)    
    p0 = sum(vec2Classify * np.log(p0Vec)) + np.log(1.0 - pClass1) # fixed at 2 classes
        
    if p1 > p0:
        return 1
    else: 
        return 0
        
        

    
##  BagOfWords
def BagOfWords():
    print "Calculating bag of words"
    print "______________________________________________"
    print ""
    
    docs = read_reuters(limit=5000)
    
    titles = []
    labels = []
    words = []
    vectors = []
    
    trainingLabels = []
    trainingVectors = []
    
    testingLabels = []
    testingVectors = []
    
    results = []
    
    a = 0
    b = 0
    c = 0
    d = 0
    
    for doc in docs:
        if "body" in doc:
            titles.append(doc["title"])
            labels.append(1 if ("usa" in doc["places"]) else 0)
            words.append(textParse(doc["body"]))
    
    
    vocab = createVocabList(words)
    
    data2 = bagOfWords2Vec(vocab,words[0])
    

    for doc in docs:
        if "body" in doc:
            titles.append(doc["title"])
            labels.append(1 if ("usa" in doc["places"]) else 0)
            words.append(textParse(doc["body"]))
    
    
    trainingVectors = words[int(len(words)*0.20):]
    trainingLabels = labels[int(len(labels)*0.20):]
    
    vocab = createVocabList(trainingVectors)
    
    
    
    for doc in trainingVectors:
        vectors+= [bagOfWords2Vec(vocab,doc)]
        
    p0v,p1v,pab = trainNB0(vectors,trainingLabels)
    
    testing_data_size = words[:int(len(words)*0.20)]
    testing_labels = labels[:int(len(labels)*0.20)]
    
    for i in range(len(testing_data_size)):
        if testing_labels[i] == 1:
            if classifyNB(bagOfWords2Vec(vocab,testing_data_size[i]),p0v,p1v,pab) ==1:
                a += 1
            else:
                b += 1
        else:
            if classifyNB(bagOfWords2Vec(vocab,testing_data_size[i]),p0v,p1v,pab) ==1:
                c += 1 
            else:
                d += 1
                
    accuracy = float(a + d)/float(a+b+c+d)
    precision = float(a)/float(a+c)
    recall = float(a)/float(a+b)
    
    print ""
    print "The accuracy for bag of words is:"+str(round(accuracy,2)*100)+" %"
    print "The precision for bag of words is:"+str(round(precision,2)*100)+" %"
    print "The recall for bag of words :"+str(round(recall,2)*100)+" %"

##  Set of words
def SetOfWords():
    print "Calculating set of words"
    print "______________________________________________"
    print ""
    
    docs = read_reuters(limit=5000)
    
    titles = []
    labels = []
    words = []
    vectors = []
    
    trainingLabels = []
    trainingVectors = []
    
    testingLabels = []
    testingVectors = []
    
    results = []
    
    a = 0
    b = 0
    c = 0
    d = 0
    
    for doc in docs:
        if "body" in doc:
            titles.append(doc["title"])
            labels.append(1 if ("usa" in doc["places"]) else 0)
            words.append(textParse(doc["body"]))
    
    
    trainingVectors = words[int(len(words)*0.20):]
    trainingLabels = labels[int(len(labels)*0.20):]
    
    vocab = createVocabList(trainingVectors)
    
    
    
    for doc in trainingVectors:
        vectors+= [setOfWords2Vec(vocab,doc)]
        
    p0v,p1v,pab = trainNB0(vectors,trainingLabels)
    
    testing_data_size = words[:int(len(words)*0.20)]
    testing_labels = labels[:int(len(labels)*0.20)]
    
    for i in range(len(testing_data_size)):
        if testing_labels[i] == 1:
            if classifyNB(setOfWords2Vec(vocab,testing_data_size[i]),p0v,p1v,pab) ==1:
                a += 1
            else:
                b += 1
        else:
            if classifyNB(setOfWords2Vec(vocab,testing_data_size[i]),p0v,p1v,pab) ==1:
                c += 1 
            else:
                d += 1
                
    accuracy = float(a + d)/float(a+b+c+d)
    precision = float(a)/float(a+c)
    recall = float(a)/float(a+b)
    
    print ""
    print "The accuracy for set of words is:"+str(round(accuracy,2)*100)+" %"
    print "The precision for set of words is:"+str(round(precision,2)*100)+" %"
    print "The recall for set of words :"+str(round(recall,2)*100)+" %"

##  Stemming
def Steming():
    print "Calculating Stemming"
    print "______________________________________________"
    print ""
    
    docs = read_reuters(limit=5000)
    
    titles = []
    labels = []
    words = []
    vectors = []
    stems = []
    
    trainingLabels = []
    trainingVectors = []
    
    testingLabels = []
    testingVectors = []
    
    results = []
    
    a = 0
    b = 0
    c = 0
    d = 0
    
    for doc in docs:
        if "body" in doc:
            titles.append(doc["title"])
            labels.append(1 if ("usa" in doc["places"]) else 0)
            words.append(textParse(doc["body"]))
    
    
    counter = 0
    for word in words:
        stemed_words = []
        for word_within in word:
            stemed_words.append(str(ps.stem(word_within)))
        words[counter] =  stemed_words
        counter = counter + 1
    
    
    trainingVectors = words[int(len(words)*0.20):]
    trainingLabels = labels[int(len(labels)*0.20):]
    
    vocab = createVocabList(trainingVectors)
    
    
    
    for doc in trainingVectors:
        vectors+= [bagOfWords2Vec(vocab,doc)]
        
    p0v,p1v,pab = trainNB0(vectors,trainingLabels)
    
    testing_data_size = words[:int(len(words)*0.20)]
    testing_labels = labels[:int(len(labels)*0.20)]
    
    for i in range(len(testing_data_size)):
        if testing_labels[i] == 1:
            if classifyNB(bagOfWords2Vec(vocab,testing_data_size[i]),p0v,p1v,pab) ==1:
                a += 1
            else:
                b += 1
        else:
            if classifyNB(bagOfWords2Vec(vocab,testing_data_size[i]),p0v,p1v,pab) ==1:
                c += 1 
            else:
                d += 1
                
    accuracy = float(a + d)/float(a+b+c+d)
    precision = float(a)/float(a+c)
    recall = float(a)/float(a+b)
    
    print ""
    print "The accuracy for set of words is:"+str(round(accuracy,2)*100)+" %"
    print "The precision for set of words is:"+str(round(precision,2)*100)+" %"
    print "The recall for set of words :"+str(round(recall,2)*100)+" %"
    
## Removing Stop words
print "Calculating Removing Stop Words"
print "______________________________________________"
print ""

docs = read_reuters(limit=5000)

stop_words = set(stopwords.words('english'))
titles = []
labels = []
words = []
words2 = []
vectors = []


trainingLabels = []
trainingVectors = []

testingLabels = []
testingVectors = []

results = []

a = 0
b = 0
c = 0
d = 0

for doc in docs:
    if "body" in doc:
        titles.append(doc["title"])
        labels.append(1 if ("usa" in doc["places"]) else 0)
        words.append(textParse(doc["body"]))


counter = 0
for word in words:
    for word_within in word:
        if word_within in stop_words:
            word.remove(word_within)

trainingVectors = words[int(len(words)*0.20):]
trainingLabels = labels[int(len(labels)*0.20):]

vocab = createVocabList(trainingVectors)



for doc in trainingVectors:
    vectors+= [bagOfWords2Vec(vocab,doc)]
    
p0v,p1v,pab = trainNB0(vectors,trainingLabels)

testing_data_size = words[:int(len(words)*0.20)]
testing_labels = labels[:int(len(labels)*0.20)]

for i in range(len(testing_data_size)):
    if testing_labels[i] == 1:
        if classifyNB(bagOfWords2Vec(vocab,testing_data_size[i]),p0v,p1v,pab) ==1:
            a += 1
        else:
            b += 1
    else:
        if classifyNB(bagOfWords2Vec(vocab,testing_data_size[i]),p0v,p1v,pab) ==1:
            c += 1 
        else:
            d += 1
            
accuracy = float(a + d)/float(a+b+c+d)
precision = float(a)/float(a+c)
recall = float(a)/float(a+b)

print ""
print "The accuracy for set of words is:"+str(round(accuracy,2)*100)+" %"
print "The precision for set of words is:"+str(round(precision,2)*100)+" %"
print "The recall for set of words :"+str(round(recall,2)*100)+" %"