<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();
    // Your own constructor code
    $this->userauth->setPage("login");
   $this->TPL['loggedin'] = $this->userauth->validSessionExists();
   $this->TPL['active'] = array('home' => false,
                                'rent'=>false,
                                'admin' => false,
                                'login'=>true,
								'products'=>false,
								'contacts'=>false
								);
	$this->TPL['user_access'] = $this->userauth->getAccesslevel();
	$this->TPL['attributes'] = array('class' => 'form','method'=>'post','accept-charset'=>'utf-8','role'=>'form','id'=>'user_info_1');
	$this->TPL['attributes'] = array('class' => 'form','method'=>'post','accept-charset'=>'utf-8','role'=>'form','id'=>'user_info_2');
	$this->TPL['forgot_pass_view'] = false;
	$this->TPL['userNotExists'] = false;
	$this->TPL['userDoesExist'] = false;
	$this->TPL['ChangePassNow']=false;
	$this->TPL['DisplayQuestions']=false;
	$this->TPL['userNotExists']=false;
	$this->TPL['security_questions_wrong']=false;
	$this->TPL['Password_not_match'] = false;
  }

  public function index()
  {
    $this->display(); 
  }

  public function loginuser()
  {
    $this->TPL['msg'] =
      $this->userauth->login($this->input->post("username"),
                             $this->input->post("password"));

    $this->template->show('login', $this->TPL);
  }

  public function logout()
  {
    $this->userauth->logout();
  }
  
  public function Forgot_pass()
  {
    $this->TPL['forgot_pass_view'] = true;
	$this->display(); 
  }
  
  public function Forgot_pass_get_user_name()
  {
		$username=$this->input->post("username");
		$query = $this->db->query("SELECT * FROM users WHERE username = '$username'");
        if($query->num_rows()==0){
            $this->TPL['userNotExists'] = true;
			$this->TPL['forgot_pass_view'] = true;
			$this->TPL['username'] = $username;
			$this->display(); 
		}else{
	  
	  
		$username=$this->input->post("username");
		$sql = "SELECT * FROM password_reset where username='$username'" ;
		$query = $this->db->query($sql);
		$result = $query->result_array()[0];
		
		$this->TPL['question_1']=$result['security_question_1'];
		$this->TPL['question_2']=$result['security_question_2'];
		$this->TPL['DisplayQuestions'] = true;
		$this->TPL['username']=$username;
		$this->display(); 
		}
  }
  
   public function Forgot_pass_reset_pass()
  {
	  $answer_1=$this->input->post("answer_1");
	  $answer_2=$this->input->post("answer_2");
	  
		$username=$this->input->post("username");
		$sql = "SELECT * FROM password_reset where username='$username'" ;
		$query = $this->db->query($sql);
		$result = $query->result_array()[0];
		if($answer_1==$result['security_answer_1'] && $answer_2==$result['security_answer_2'])
		{
			$this->TPL['username']=$username;
			$this->TPL['ChangePassNow']=true;
			$this->display(); 
		}else{
			$username=$this->input->post("username");
			$sql = "SELECT * FROM password_reset where username='$username'" ;
			$query = $this->db->query($sql);
			$result = $query->result_array()[0];
			$this->TPL['question_1']=$result['security_question_1'];
			$this->TPL['question_2']=$result['security_question_2'];
			$this->TPL['username']=$username;
			$this->TPL['ChangePassNow']=false;
			$this->TPL['security_questions_wrong']=true;
			$this->TPL['DisplayQuestions'] = true;
			$this->display(); 
		}
	  
  }
  
  public function Password_Reset_now()
  {
	$username=$this->input->post("username");
	$new_password=$this->input->post("password");
	$conf_new_pass=$this->input->post("ConfirmNewPass");
	
	if($new_password==$conf_new_pass)
	{
		$query = $this->db->query("UPDATE users SET password = '$new_password' WHERE username = '$username'");
		$this->TPL['Pass_changed_successfully']=true;
		$this->template->show('login', $this->TPL);
	}else{
		$this->TPL['Password_not_match'] = true;
		$this->TPL['username']=$username;
		$this->TPL['ChangePassNow']=true;
		$this->display(); 
	}
  }
  
   public function display()
	{
		$this->template->show('login', $this->TPL);
	}
}
?>