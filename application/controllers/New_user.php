<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class New_user extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();
    // Your own constructor code
    $this->TPL['loggedin'] = $this->userauth->validSessionExists();
	$this->TPL['username'] = $this->userauth->getUsername();
	$this->TPL['user_access'] = $this->userauth->getAccesslevel();
       $this->TPL['active'] = array('home' => false,
                                'rent'=>false,
                                'admin' => true,
                                'login'=>false,
								'products'=>false,
								'new_user'=>false,
								'contacts'=>false);
								
								
	$this->TPL['attributes'] = array('class' => 'form','method'=>'post','accept-charset'=>'utf-8','role'=>'form','id'=>'user_info_1');
	$this->TPL['attributes'] = array('class' => 'form','method'=>'post','accept-charset'=>'utf-8','role'=>'form','id'=>'user_info_2');	
    $this->TPL['error'] = false;
    $this->TPL['userExists'] = false;
	$this->TPL['CreatedUser_section_1'] = false;
	
  }
  public function index()
  {
	$this->load->library('form_validation');
    $this->display();
    
  }
  public function Create_User_info_1(){
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
    $this->form_validation->set_rules('username', 'username', 'required',array('required'=>'The Username field is required.'));
    $this->form_validation->set_rules('password', 'password', 'required',array('required'=>'The Password field is required.'));
	$this->form_validation->set_rules('ConfirmNewPass', 'Password Confirmation', 'required|matches[password]',array('required'=>'Confirm Password does not match New password'));
   
    if ($this->form_validation->run() == FALSE)
    {
      $this->TPL['error'] = true;
	  $this->display();
    }else{
		$first_name = $this->input->post("first_name");
		$last_name = $this->input->post("last_name");
        $user_name = $this->input->post("username");
		$email = $this->input->post("email");
        $password = $this->input->post("password");
		$ConfirmNewPass = $this->input->post("ConfirmNewPass");
		$month = $this->input->post("month");
		$day = $this->input->post("day");
		$year = $this->input->post("year");

        $query = $this->db->query("SELECT * FROM users WHERE username = '$user_name'");
        if($query->num_rows()>0){
            $this->TPL['userExists'] = true;
			$this->display();
        }else{
          //$query = $this->db->query("INSERT INTO users(username,password,accesslevel,frozen) VALUES ('$username', '$password', 'member', 'N');");
		  $this->TPL['first_name'] = $this->input->post("first_name");
		  $this->TPL['last_name'] = $this->input->post("last_name");
		  $this->TPL['user_name'] = $user_name;
		  $this->TPL['email'] =  $this->input->post("email");
		  $this->TPL['password'] = $this->input->post("password");
		  $this->TPL['ConfirmNewPass'] = $this->input->post("ConfirmNewPass");
		  $this->TPL['month'] = $this->input->post("month");
		  $this->TPL['day'] = $this->input->post("day");
		  $this->TPL['year'] =  $this->input->post("year");

		  $this->TPL['CreatedUser_section_1'] = true;
		  $this->display();
        }
       
    }
  }
  
  public function Create_User_info_2(){
	
    
		$first_name = $this->input->post("first_name");
		$last_name = $this->input->post("last_name");
        $user_name = $this->input->post("user_name");
		$email = $this->input->post("email");
        $password = $this->input->post("password");
		$ConfirmNewPass = $this->input->post("ConfirmNewPass");
		$month = $this->input->post("month");
		$day = $this->input->post("day");
		$year = $this->input->post("year");
		$birthday = $year . "-" . $month . "-" . $day;
		
		$question_1 = $this->input->post("question_1");
		$answer_1 = $this->input->post("answer_1");
		$question_2 = $this->input->post("question_2");
		$answer_2 = $this->input->post("answer_2");
		
		

		$query = $this->db->query("INSERT INTO users(username,password,accesslevel,frozen) VALUES ('$user_name', '$password', 'member', 'N');");
        $query = $this->db->query("INSERT INTO user_profile(first_name,last_name,email,username,birthday) VALUES ('$first_name', '$last_name', '$email', '$user_name','$birthday');");
		$query = $this->db->query("INSERT INTO password_reset(username,security_question_1,security_answer_1,security_question_2,security_answer_2) VALUES ('$user_name', '$question_1', '$answer_1', '$question_2','$answer_2');");
		$this->TPL['CreatedUser_section_1'] = false;
        
		
		$path = "./application/assets/img/".$user_name;		
		if(!is_dir($path)) //create the folder if it's not already exists
		{
			mkdir($path,0755,TRUE);
		}
		$this->TPL['User_Created'] = true;
         $this->template->show('login', $this->TPL);
    
	  
  }
  
    public function display()
	{
		$this->template->show('new_user', $this->TPL);
	}
}
?>