<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Term_of_use extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();

   $this->TPL['loggedin'] = $this->userauth->validSessionExists();
   $this->TPL['active'] = array('home' => false,
                                'rent'=>false,
                                'admin' => false,
                                'login'=>true,
								'products'=>false,
								'contacts'=>false
								);

  }

  public function index()
  {
    $this->display(); 
  }

   public function display()
	{
		$this->template->show('term_of_use', $this->TPL);
	}
}
?>