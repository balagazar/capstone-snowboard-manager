<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();
    // Your own constructor code
    $this->TPL['contacts'] = true;
	$this->userauth->setPage("Contact");
	$this->TPL['username'] = $this->userauth->getUsername();
	   $this->TPL['loggedin'] = $this->userauth->validSessionExists();
	   $this->TPL['user_access'] = $this->userauth->getAccesslevel();
	   $this->TPL['active'] = array('home' => false,
                                'rent'=>false,
                                'admin' => false,
                                'login'=>false,
								'products'=>false,
								'contacts'=>true
								);
	$this->TPL['success'] = false;
  }

  public function index()
  {
    $TPL['contacts'] = true;
    $this->display();
  }
  
  
  public function display()
  {
	  
	  $this->template->show('Contact', $this->TPL);
  }
  
  
  	public function Contact_admin()
	{
		
			$from=$this->userauth->getUsername();
			$content=$this->input->post("Message_Content");
			$date = date("Y-m-d");
			$loggedin = $this->userauth->validSessionExists();
			
			if(!$loggedin){
			$query = $this->db->query("INSERT INTO message(message_from,message_to,message_date,message_time,was_message_read,message_content,message_subject) VALUES ('public', 'admin', '$date',NOW(),'N', '$content','public issue');");
			}else{
			$query = $this->db->query("INSERT INTO message(message_from,message_to,message_date,message_time,was_message_read,message_content,message_subject) VALUES ('$from', 'admin', '$date',NOW(),'N', '$content','user issue');");
			}
			$this->TPL['success'] = true;
			
			$this->template->show('Contact', $this->TPL);
	}

}
?>