<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();
    // Your own constructor code
	$this->TPL['home'] = true;
    $this->userauth->setPage("home");
    $this->TPL['loggedin'] = $this->userauth->validSessionExists();
	$this->TPL['user_access'] = $this->userauth->getAccesslevel();
	$this->TPL['username'] = $this->userauth->getUsername();
      $this->TPL['active'] = array('home' => true,
                                'rent'=>false,
                                'admin' => false,
                                'login'=>false,
								'products'=>false,
								'contacts'=>false
								);

  }

  public function index()
  {
    $this->display();
  }
  public function find($string = null){
    if($string != null && strlen($string) > 0){
      $sql = "SELECT * FROM users WHERE username LIKE '%$string%' and accesslevel = 'member'";

      $query = $this->db->query($sql);
      $table = $query->result_array();
      echo json_encode($table);
    }
    
  }
  
 public function display(){

    $this->template->show('home', $this->TPL);
  }

}
?>