<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Profile extends CI_Controller {

  var $TPL;

  public function __construct()
  {
	
    parent::__construct();
    // Your own constructor code
	$this->load->helper(array('form', 'url','file'));
	date_default_timezone_set('UTC');
	$this->userauth->setPage("user_profile");
	$this->TPL['loggedin'] = $this->userauth->loggedin();
	$this->TPL['username'] = $this->userauth->getUsername();
	$this->TPL['user_access'] = $this->userauth->getAccesslevel();
	 $this->TPL['PassView'] =false;
  }
  public function index()
  {
	
    $this->display();
  }
  

  public function display(){
	$username=$this->userauth->getUsername();
	$filename = './application/assets/img/'.$username.'/profile_pic/profile_picture.jpg';
	if (file_exists($filename)) {
		 $this->TPL['profile_pic_exist']=true;
	} else {
		$this->TPL['profile_pic_exist']=false;
	}
	
	$username=$this->userauth->getUsername();
	$sql = "SELECT * FROM user_profile where username='$username'" ;
    $query = $this->db->query($sql);
	$this->TPL['result'] = $query->result_array()[0];
	
    $this->template->show('user_profile', $this->TPL);
  }
  

  public function do_upload()
        {
				
				$this->load->helper('file');
				$username=$this->userauth->getUsername();
				
				$path = "./application/assets/img/".$username."/profile_pic";
				
				if(!is_dir($path)) //create the folder if it's not already exists
				{
				  mkdir($path,0755,TRUE);
				}
				
				$thumb_path = "./application/assets/img/".$username."/profile_pic/thumb";
				if(!is_dir($thumb_path)) //create the folder if it's not already exists
				{
				  mkdir($thumb_path,0755,TRUE);
				}
				
				delete_files('./application/assets/img/'.$username.'/profile_pic/');

				
                $config['upload_path']          = './application/assets/img/'.$username.'/profile_pic/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;
				$config['file_name']           = "profile_picture.jpg";
				

                $this->load->library('upload', $config);
				$this->upload->initialize($config);
				
		
                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array('error' => $this->upload->display_errors());
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $this->TPL['uploaded_successfully']="The picture was uploaded successfully";
                }
				
				
				$config['image_library'] = 'gd2';
				$config['source_image'] = './application/assets/img/'.$username.'/profile_pic/profile_picture.jpg';
				$config['new_image'] = './application/assets/img/'.$username.'/profile_pic/thumb/';
				$config['create_thumb'] = TRUE;
				$config['thumb_marker'] = '_thumb';
				$config['maintain_ratio'] = TRUE;
				$config['width']         = 75;
				$config['height']       = 50;
	
				

				$this->load->library('image_lib', $config);

				   $this->image_lib->resize();
					
					// clear //
					$this->image_lib->clear();
				
				$this->upload->initialize($config);
			$this->display();	
        }
		
		public function DeletePhoto()
		{
			$username=$this->userauth->getUsername();
			delete_files('./application/assets/img/'.$username.'/profile_pic/');
			$this->display();
		}
		public function ChangePassView()
		{
			$this->TPL['PassView'] =true;
			$this->display();
		}
		public function change_snowboard_size(){
			$weight = $this->input->post("weight");
			switch ($weight) {
						case 80:
							echo "i equals 0";
							break;
						case 1:
							echo "i equals 1";
							break;
						case 2:
							echo "i equals 2";
							break;
					}
		}
		public function ChangeSecurityView()
		{
			$username=$this->userauth->getUsername();
			$this->TPL['SecurityView'] =true;
			$sql = "SELECT * FROM password_reset where username='$username'" ;
			$query = $this->db->query($sql);
			$this->TPL['result'] = $query->result_array()[0];
			$this->template->show('user_profile', $this->TPL);
		}
		public function  ChangePassWord()
		{
			$username=$this->userauth->getUsername();//Get username info
			$query = $this->db->query("SELECT * FROM users WHERE username = '$username'");
			$row = $query->result_array()[0];
			$CurrentPasswordDB=$row['password'];
			
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$this->form_validation->set_rules('CurrentPass', 'Current Password', 'required|callback_password_check',array('required'=>'The Password field is required.'));
			$this->form_validation->set_rules('NewPass', 'New Passowrd', 'required',array('required'=>'The Password field is required.'));
			$this->form_validation->set_rules('ConfirmNewPass', 'Password Confirmation', 'required|matches[NewPass]',array('required'=>'Confirm Password does not match New password'));
			
			$CurrentPassword = $this->input->post("CurrentPass");
			$NewPass = $this->input->post("NewPass");
			$ConfirmNewPass = $this->input->post("ConfirmNewPass");
			 if ($this->form_validation->run() == FALSE)
			{
			  $this->TPL['error'] = true;
			  $this->TPL['PassView'] =true;
			}else{
				  $query = $this->db->query("UPDATE users SET password = '$NewPass' WHERE username = '$username'");
				  $this->TPL['success'] = true;
				  $this->TPL['PassView'] =false;
				}
				
			
			$this->display();
			
		}
		 public function password_check($str)
        {
			$username=$this->userauth->getUsername();//Get username info
			$query = $this->db->query("SELECT * FROM users WHERE username = '$username'");
			$row = $query->result_array()[0];
			$CurrentPasswordDB=$row['password'];
                if ($str !=$CurrentPasswordDB)
                {
                        $this->form_validation->set_message('CurrentPass_check', 'Current Passowrd is incorrect"');
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }
		public function SubmitChanges()
		{
			$username=$this->userauth->getUsername();//Get username info
			$email= $this->input->post("email");
			$address=$this->input->post("address");
			$country=$this->input->post("country");
			$snowboardType=$this->input->post("snowboardType");
			$birthday=$this->input->post("birthday");
			$about=$this->input->post("about");
			
			 $query = $this->db->query("UPDATE user_profile SET email = '$email' WHERE username = '$username'");
			 $query = $this->db->query("UPDATE user_profile SET address = '$address' WHERE username = '$username'");
			 $query = $this->db->query("UPDATE user_profile SET country = '$country' WHERE username = '$username'");
			 $query = $this->db->query("UPDATE user_profile SET snowboardType = '$snowboardType' WHERE username = '$username'");
			 $query = $this->db->query("UPDATE user_profile SET birthday = '$birthday' WHERE username = '$username'");
			 $query = $this->db->query("UPDATE user_profile SET about = '$about' WHERE username = '$username'");
			 
			 $this->display();
		}
		public function SecurityQuestionChange()
		{
			$username=$this->userauth->getUsername();//Get username info
			$question_1= $this->input->post("question_1");
			$answer_1= $this->input->post("answer_1");
			$question_2= $this->input->post("question_2");
			$answer_2= $this->input->post("answer_2");
			
			$query = $this->db->query("UPDATE password_reset SET security_question_1 = '$question_1' WHERE username = '$username'");
			$query = $this->db->query("UPDATE password_reset SET security_answer_1 = '$answer_1' WHERE username = '$username'");
			$query = $this->db->query("UPDATE password_reset SET security_question_2 = '$question_2' WHERE username = '$username'");
			$query = $this->db->query("UPDATE password_reset SET security_answer_2 = '$answer_2' WHERE username = '$username'");
			
			
			$this->display();
		}
}
?>