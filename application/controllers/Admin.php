<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();
    // Your own constructor code
    $this->userauth->setPage("admin");
    $this->TPL['loggedin'] = $this->userauth->loggedin();
	$this->TPL['username'] = $this->userauth->getUsername();
	$this->TPL['user_access'] = $this->userauth->getAccesslevel();
       $this->TPL['active'] = array('home' => false,
                                'rent'=>false,
                                'admin' => true,
                                'login'=>false,
								'products'=>false,
								'contacts'=>false
								);
	$this->TPL['admin_orders'] = false;
    $this->TPL['error'] = false;
    $this->TPL['userExists'] = false;
	$this->TPL['orders_view'] = false;
	$this->TPL['reports_view'] = false;
	$this->TPL['messages_log_view'] = false;
	$this->TPL['products_view'] = false;
	$this->TPL['rents_view'] = false;
	
  }
  public function display(){
    $sql = "SELECT * FROM users";
    $query = $this->db->query($sql);
    $this->TPL['listing'] = $query->result_array();
    $this->template->show('admin', $this->TPL);
  }
  public function index()
  {
    $this->display();
    $this->load->library('form_validation');
  }
  public function check_accesslevel($str){
    if($str == "member" || $str == "admin"){
      return true;
    }else{
      return false;
    }
  }
  public function adduser(){
    $this->load->helper(array('form', 'url'));
    $this->load->library('form_validation');
    $this->form_validation->set_rules('username', 'username', 'required',array('required'=>'The Username field is required.'));
    $this->form_validation->set_rules('password', 'password', 'required',array('required'=>'The Password field is required.'));
    $this->form_validation->set_rules('accesslevel', 'accesslevel', 'required|callback_check_accesslevel',array('required'=>'Access level must be either member or admin.',
                                                                      'check_accesslevel'=>'Access level must be either member or admin.'));
    if ($this->form_validation->run() == FALSE)
    {
      $this->TPL['error'] = true;
    }else
    {
        $username = $this->input->post("username");
        $password = $this->input->post("password");
        $accesslevel = $this->input->post("accesslevel");
        $query = $this->db->query("SELECT * FROM users WHERE username = '$username'");
        if($query->num_rows()>0){
            $this->TPL['userExists'] = true;
        }else{
          $query = $this->db->query("INSERT INTO users(username,password,accesslevel,frozen) VALUES ('$username', '$password', '$accesslevel', 'N');");
        }
        
    }
    $this->display();
  }
  public function delete($id){
    $query = $this->db->query("DELETE FROM users WHERE user_id = $id");
    $this->display();
  }
  public function delete_byname($name){
    $query = $this->db->query("DELETE FROM users WHERE username = '$name'");
    $this->display();
  }
  public function delete_product()
  {
	  $this->TPL['products_view'] = true;
	  $id = $this->input->get_post('submit');
	  $query = $this->db->query("DELETE FROM products WHERE product_id = '$id'");
    $this->products_view();
  }
  public function freeze($id){
    $query = $this->db->query("SELECT frozen FROM users WHERE user_id = $id");
    $row = $query->result_array()[0];
    if($row['frozen'] == "N"){
      $this->db->query("UPDATE users SET frozen = 'Y' WHERE user_id = $id");
    }else{
      $this->db->query("UPDATE users SET frozen = 'N' WHERE user_id = $id");
    }
    $this->display();
  }
  public function freeze_byname($name){
      $this->db->query("UPDATE users SET frozen = 'Y' WHERE username = '$name'");
	 $this->display();
  }
  
  public function Orders_view(){
	 $this->TPL['orders_view'] = true;
    $sql = "SELECT * FROM orders";
    $query = $this->db->query($sql);
    $this->TPL['orders'] = $query->result_array();
    $this->template->show('admin', $this->TPL);
  }
  public function reports_view()
  {
	  $this->TPL['reports_view'] = true;
	$sql = "SELECT * FROM reports";
    $query = $this->db->query($sql);
    $this->TPL['reports'] = $query->result_array();
    $this->template->show('admin', $this->TPL);
  }
  
  

  
  
  
   public function Rent_view()
  {
	   $this->TPL['rents_view'] = true;
	$sql = "select * from rent
inner join rent_sizes on rent.rent_id = rent_sizes.item_id
inner join rent_dates on rent_dates.id = rent_sizes.id";
    $query = $this->db->query($sql);
    $this->TPL['rents'] = $query->result_array();
    $this->template->show('admin', $this->TPL);
  }
  
     public function Rent_inv_view()
  {
	   $this->TPL['Rent_inv_view'] = true;
	$sql = "select * from rent
inner join rent_sizes on rent.rent_id = rent_sizes.item_id";
    $query = $this->db->query($sql);
    $this->TPL['rent_inv'] = $query->result_array();
    $this->template->show('admin', $this->TPL);
  }
  
  
   public function delete_rent_item()
  {
	   $id = $this->input->get_post(submit);
		$sql = "delete from rent_sizes where id = '$id'";
		$query = $this->db->query($sql);
		
		$this->Rent_inv_view();
  }
  
  
  
   public function delete_rent()
  {
	   $id = $this->input->get_post(submit);
		$sql = "delete from rent_dates where id = '$id'";
		$query = $this->db->query($sql);
		
		$this->Rent_view();
  }
  
  
  public function products_view(){
	  $this->TPL['products_view'] = true;
	$sql = "SELECT * FROM products";
    $query = $this->db->query($sql);
    $this->TPL['products'] = $query->result_array();
    $this->template->show('admin', $this->TPL);
  }
  public function delete_report(){
    $sql = "delete from reports where report_id = '$id '";
    $query = $this->db->query($sql);
	$this->reports_view();
  }
  

public function messaging_log_view()
  {
	  $this->TPL['messages_log_view'] = true;
	$sql = "SELECT * FROM message";
    $query = $this->db->query($sql);
    $this->TPL['messages'] = $query->result_array();
    $this->template->show('admin', $this->TPL);
  }
  public function update($field){
    $id = $this->input->get_post('pk');
    $fieldData = $this->input->get_post('value');
    $sql = "UPDATE orders SET $field = '$fieldData' WHERE order_id = '$id'";
    $query = $this->db->query($sql);
  }
  
   public function delete_order(){
    $id = $this->input->get_post(submit);
    $sql = "delete from orders where order_id = '$id '";
    $query = $this->db->query($sql);
	$this->Orders_view();
  }
  
  public function delete_message(){
    $id = $this->input->get_post(submit);
    $sql = "delete from message where message_id = '$id '";
    $query = $this->db->query($sql);
	$this->messaging_log_view();
  }
  
}
?>