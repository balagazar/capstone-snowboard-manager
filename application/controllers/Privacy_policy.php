<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privacy_Policy extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();
    // Your own constructor code
   $this->TPL['loggedin'] = $this->userauth->validSessionExists();

  }

  public function index()
  {
    $this->display(); 
  }

   public function display()
	{
		$this->template->show('privacy_policy', $this->TPL);
	}
}
?>