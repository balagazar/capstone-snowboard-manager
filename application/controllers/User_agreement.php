<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Agreement extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();
    // Your own constructor code
    $this->userauth->setPage("login");
   $this->TPL['loggedin'] = $this->userauth->validSessionExists();
   $this->TPL['active'] = array('home' => false,
                                'rent'=>false,
                                'admin' => false,
                                'login'=>true,
								'products'=>false,
								'contacts'=>false
								);
  }

  public function index()
  {
    $this->display(); 
  }

  
   public function display()
	{
		$this->template->show('user_agreement', $this->TPL);
	}
}
?>