<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rent extends CI_Controller {

  var $TPL;

  public function __construct()
  {
	  
    parent::__construct();
    // Your own constructor code
	date_default_timezone_set('UTC');
	  $this->userauth->setPage("rent");
    $this->TPL['loggedin'] = $this->userauth->validSessionExists();
	$this->TPL['username'] = $this->userauth->getUsername();
	$this->TPL['user_access'] = $this->userauth->getAccesslevel();
   $this->TPL['active'] = array('home' => false,
                                'rent'=>true,
                                'admin' => false,
                                'login'=>false,
								'products'=>false,
								'contacts'=>false
								);
    $this->TPL['rent_page'] = true;
	$this->TPL['have_rented_items'] = false;
	$this->TPL['rent_days'] = 0;
	$this->TPL['Helmet_Size_options'] = array(
		'select' => 'select',
        'small'         => 'Small',
        'med'           => 'Medium',
        'large'         => 'Large',
);
	$this->TPL['Snowboard_Size_Options'] = array(
		'select' => 'select',
        '152'         => '152',
        '153'           => '153',
        '155'         => '155',
		'157'         => '157',
		'159'         => '159',
		'160'         => '160',
		'162'         => '162',
		'163'         => '163',
);
$this->TPL['Boots_Size_Options'] = array(
		'select' => 'select',
        '7'         => '7',
        '8'           => '8',
       '9'           => '9',
	   '10'           => '10',
	   '11'           => '11',
	   '12'           => '12',
	   '13'           => '13',
);
$this->TPL['Gloves_Size_options'] = array(
		'select' => 'select',
        'small'         => 'Small',
        'med'           => 'Medium',
        'large'         => 'Large',
);
  }

  public function index()
  {
    $TPL['rent'] = true;
    $this->display();
  }
  
  public function display(){
		$username = $this->userauth->getUsername();
		$sql = "select * from rent
		inner join rent_sizes on rent.rent_id=rent_sizes.item_id
		inner join rent_dates on rent_sizes.id = rent_dates.id
		where rent_dates.rented_by = '$username'";
		
		$TPL['today_date'] = date('Y-m-d');
		
		$query = $this->db->query($sql);
			if($query->num_rows()>0){
				$this->TPL['have_rented_items'] = true;
				$this->TPL['rented_items'] = $query->result_array();
			}
    $this->template->show('rent', $this->TPL);
  }
  
  
  public function check_availability()
  {
		$item =  $this->uri->segment(3);
		$size =  $this->uri->segment(4); 
		$date_check_in =  $this->uri->segment(5);
		$date_return =  $this->uri->segment(6); 
		

		if($item =='snowboards' || $item =='boots')
		{
			$size = (int)$size;
		}
			$query = $this->db->query("select * from rent
			inner join rent_sizes on rent.rent_id = rent_sizes.item_id
			where rent.Category = '$item' and rent_sizes.item_size = '$size' and rent_sizes.id not in (SELECT rent_dates.id FROM rent
				INNER JOIN rent_sizes ON rent.rent_id=rent_sizes.item_id inner 
				JOIN rent_dates on rent_sizes.id = rent_dates.id 
				where rent.Category = '$item' and rent_sizes.item_size ='$size' and  '$date_check_in' <= rent_dates.check_in_date and '$date_return' >= rent_dates.return_date 
				or rent_dates.return_date = '$date_check_in' 
				or '$date_check_in' >= rent_dates.check_in_date and '$date_return' <= rent_dates.return_date 
				or '$date_check_in' <= rent_dates.return_date 
				or '$date_return' >= rent_dates.check_in_date and '$date_check_in' <= rent_dates.check_in_date)");

			if($query->num_rows()>0){	//if sql finds an item that is not rented between requested dates and available to rent
					$rent_info = $query->result_array()[0];
					echo json_encode($rent_info);
					
			}else{
					$query = $this->db->query("SELECT * FROM rent
			INNER JOIN rent_sizes ON rent.rent_id=rent_sizes.item_id inner 
			JOIN rent_dates on rent_sizes.id = rent_dates.id 
			where rent.Category = '$item' and rent_sizes.item_size ='$size'
			order by ABS( DATEDIFF(rent_dates.return_date,'$date_check_in'))
			");
					$rent_info = $query->result_array()[0];
					echo json_encode($rent_info);
			}
		
  }
  
	public function rent_item(){
		$username = $this->userauth->getUsername();
		$rent_item_id =  $this->uri->segment(3); 
		$date_check_in =  $this->uri->segment(4);
		$date_return =  $this->uri->segment(5); 
		
		$sql = "INSERT INTO rent_dates(check_in_date,return_date,rented_by,id) values('$date_check_in','$date_return','$username','$rent_item_id')";
		$query = $this->db->query($sql);
		echo json_encode("success");
	}
  
  
 
  
}
?>