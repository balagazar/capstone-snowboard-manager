<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller {

  var $TPL;

  public function __construct()
  {
    parent::__construct();
    // Your own constructor code
    $this->TPL['loggedin'] = $this->userauth->loggedin();
	$this->TPL['current_logged_username'] = $this->userauth->getUsername();
	$this->TPL['user_access'] = $this->userauth->getAccesslevel();
    $this->TPL['active'] = array('home' => false,
                                'rent'=>false,
                                'admin' => true,
                                'login'=>false,
								'products'=>false,
								'new_user'=>false,
								'contacts'=>false);
														
    $this->TPL['error'] = false;
	$this->TPL['orders_empty'] = false;
	

	
  }
  public function index()
  {
	$this->load->library('form_validation');
    $this->display();
    
  }
  

    public function display()
	{
		$username = $this->userauth->getUsername();

		$sql = "SELECT orders.shipping_address,orders.status,orders.city,orders.province,orders.product_id,orders.country,orders.postal_code,orders.username, products.product_picture, products.product_name,orders.quantity FROM orders  INNER JOIN products ON orders.product_id=products.product_id where username = '$username'";
		$query = $this->db->query($sql);
		$this->TPL['orders'] = $query->result_array();
		if($query->num_rows()==0){
			$this->TPL['orders_empty'] = true;
			$this->template->show('orders', $this->TPL);
		}else{
			$this->template->show('orders', $this->TPL);
		}
	}
	

	

	

	
}
?>