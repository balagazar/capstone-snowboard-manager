<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Userauth  { 
	  
    private $login_page = "";   
    private $logout_page = "";
    private $redirect_page = "";   
     
    private $username;
    private $password;

    private $accesslevel = "public";
    private $page = "Home";

    /**
    * Turn off notices so we can have session_start run twice
    */
    function __construct() 
    {
      error_reporting(E_ALL & ~E_NOTICE);
      $this->login_page = base_url() . "index.php?/Login";
      $this->logout_page = base_url() . "index.php?/Home";
      $this->redirect_page = base_url() . "index.php?/";
    }

    /**
    * @return string
    * @desc Login handling
    */
    public function login($username,$password) 
    {

      session_start();
        
      // User is already logged in if SESSION variables are good. 
      if ($this->validSessionExists() == true)
      {
        $this->redirect($_SESSION['basepage']);
      }

      // First time users don't get an error message.... 
      if ($_SERVER['REQUEST_METHOD'] == 'GET') return;
        
      // Check login form for well formedness.....if bad, send error message
      if ($this->formHasValidCharacters($username, $password) == false)
      {
         return "Username/password fields cannot be blank!";
      }
        
      // verify if form's data coresponds to database's data
      if ($this->userIsInDatabase() == false)
      {
        return 'Invalid username/password!';
      }else if ($this->userIsInDatabase() === "Account frozen!") {
        return "Account frozen!";
      }
      else
      { 
        // We're in!
        // Redirect authenticated users to the correct landing page
        // ex: admin goes to admin, members go to members
        $this->writeSession();
        $this->redirect($_SESSION['basepage']);
      }
    }
    public function getAccesslevel(){
          if(isset($_SESSION['accesslevel'])){
            return $_SESSION['accesslevel'];
          }else{
            return 'public';
          }
    }
    public function checkACL(){
        $CI =& get_instance();
        $aclFile = $CI->config->item('acl');
        $acl = $aclFile[strtolower($this->page)][$this->getAccesslevel()];
        if($acl == true){
          return true;
        }
        return false;
        
    }
    
	   public function setPage($page){
        $this->page = $page;
     }
    /**
    * @return void
    * @desc Validate if user is logged in
    */
    public function loggedin() 
    {

      session_start();     
      // Users who are not logged in are redirected out
      if ($this->validSessionExists() == false)
      {
        $this->redirect($this->login_page);
      }
      if ($this->checkACL() == false) {
        if ($_SESSION['accesslevel'] == "public") {
          $this->redirect($this->redirect_page."Home");
        }else if($_SESSION['accesslevel'] == "member"){
          $this->redirect($this->redirect_page.ucfirst("home"));
        }else{
          $this->redirect($this->redirect_page.ucfirst($_SESSION['accesslevel']));
        }
        
      }
		 
      // Access Control List checking goes here..
      // Does user have sufficient permissions to access page?
      // Ex. Can a bronze level access the Admin page?   

      
      return true;
    }
	
    /**
    * @return void
    * @desc The user will be logged out.
    */
    public function logout() 
    {
      session_start(); 
      $_SESSION = array();
      session_destroy();
      header("Location: ".$this->logout_page);
    }
    
    /**
    * @return bool
    * @desc Verify if user has got a session and if the user's IP corresonds to the IP in the session.
    */
    public function validSessionExists() 
    {
      session_start();
      if (!isset($_SESSION['username']))
      {
        return false;
      }
      else
      {
        return true;
      }
    }
    
    /**
    * @return void
    * @desc Verify if login form fields were filled out correctly
    */
    public function formHasValidCharacters($username, $password) 
    {
      // check form values for strange characters and length (3-12 characters).
      // if both values have values at this point, then basic requirements met
      if ( (empty($username) == false) && (empty($password) == false) )
      {
        $this->username = $username;
        $this->password = $password;
        return true;
      }
      else
      {
        return false;
      }
    }
	
    /**
    * @return bool
    * @desc Verify username and password with MySQL database.
    */
    public function userIsInDatabase() 
    {

      // Remember: you can get CodeIgniter instance from within a library with:
      // $CI =& get_instance();
      // And then you can access database query method with:
      // $CI->db->query()
        
      // Access database to verify username and password from database table
      $sql = "SELECT * FROM users WHERE username = '".$this->username."' AND password = '".$this->password."'";
      $CI =& get_instance();
      $query = $CI->db->query($sql);
      if ($query->num_rows() > 0)  
      {    
        $row = $query->result_array()[0];
        $frozen = $row['frozen'];
        if($frozen == "N"){
          $this->accesslevel = $row['accesslevel'];
          return true;
        }else{
          return "Account frozen!";
        }
        
      } 
      else 
      {
        return false; 
      }
    }
    
    
    /**
    * @return void
    * @param string $page
    * @desc Redirect the browser to the value in $page.
    */
    public function redirect($page) 
    {
        header("Location: ".$page);
        exit();
    }
    
    /**
    * @return void
    * @desc Write username and other data into the session.
    */
    public function writeSession() 
    {
        $_SESSION['username'] = $this->username;
        $_SESSION['accesslevel'] = $this->accesslevel;
        if($_SESSION['accesslevel'] == "member"){
          $_SESSION['basepage'] = base_url() . "index.php?/Home";
        }else{
          $_SESSION['basepage'] = base_url() . "index.php?/Home";
        }
        
        
    }
	
    /**
    * @return string
    * @desc Username getter, not necessary 
    */
    public function getUsername() 
    {
        return $_SESSION['username'];
    }
		 
}
?>