<div id="body">
<div class="container" style="background-color:white;">
    <div class="row">
		<?php if(!$product_cart_empty){ ?>
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <table class="table table-hover">
			<
			<h1>My Shopping Cart</h1>
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Total</th>
						<?= $product['username'] ?>
						<?= $product['recipient']?>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
				 
				<?php foreach($shopping_cart as $product) { ?>
					<tr>
							<td class="col-sm-8 col-md-6">
							<div class="media">
								<a class="thumbnail pull-left" href="<?= base_url(); ?>index.php?/Products/Single_Product/<?= $product['product_id'] ?>"><img class="media-object" src="<?= assetUrl();?><?= $product['product_picture']?>" style="width: 72px; height: 72px;"> </a>
								<div class="media-body">
									<h4 class="media-heading"><a href="<?= base_url(); ?>index.php?/Products/Single_Product/<?= $product['product_id'] ?>"><?= $product['product_name'] ?></a></h4>
								</div>
								
							</div></td>
							<td class="col-sm-1 col-md-1" style="text-align: center">
							<select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
								<option name="select" value="">Select...</option>
								<option name="1" <?php if($product['quantity'] == 1){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/1/<?= $product['product_id'] ?>">1</option>
								<option name="2" <?php if($product['quantity'] == 2){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/2/<?= $product['product_id'] ?>">2</option>
								<option name="3" <?php if($product['quantity'] == 3){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/3/<?= $product['product_id'] ?>">3</option>
								<option name="4" <?php if($product['quantity'] == 4){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/4/<?= $product['product_id'] ?>">4</option>
								<option name="5" <?php if($product['quantity'] == 5){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/5/<?= $product['product_id'] ?>">5</option>
								<option name="6" <?php if($product['quantity'] == 6){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/6/<?= $product['product_id'] ?>">6</option>
								<option name="7" <?php if($product['quantity'] == 7){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/7/<?= $product['product_id'] ?>">7</option>
								<option name="8" <?php if($product['quantity'] == 8){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/8/<?= $product['product_id'] ?>">8</option>
								<option name="9" <?php if($product['quantity'] == 9){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/9/<?= $product['product_id'] ?>">9</option>
								<option name="10" <?php if($product['quantity'] == 10){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/10/<?= $product['product_id'] ?>">10</option>
								<option name="11" <?php if($product['quantity'] == 11){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/11/<?= $product['product_id'] ?>">11</option>
								<option name="12" <?php if($product['quantity'] == 12){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/12/<?= $product['product_id'] ?>">12</option>
								<option name="13" <?php if($product['quantity'] == 13){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/13/<?= $product['product_id'] ?>">13</option>
								<option name="14" <?php if($product['quantity'] == 14){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/14/<?= $product['product_id'] ?>">14</option>
								<option name="15" <?php if($product['quantity'] == 15){ ?> selected <?php } ?> value="<?= base_url(); ?>index.php?/Shopping_cart/change_total/15/<?= $product['product_id'] ?>">15</option>
							</select>
							</td>
							<td class="col-sm-1 col-md-1 text-center"><strong>$<?= $product['product_price'] ?></strong></td>
							<td class="col-sm-1 col-md-1 text-center" id ="quantity_after_ajax"><strong>$<?= $product['total'] ?></strong></td>
							<td class="col-sm-1 col-md-1">
							<a type="button" class="btn btn-danger" href="<?= base_url(); ?>index.php?/Shopping_cart/delete_item/<?= $product['product_id'] ?>">
								<span class="glyphicon glyphicon-remove"></span> Remove
							</a></td>
					</tr>
				<?php } ?>
						<tr>
							<td>   </td>
							<td>   </td>
							<td>   </td>
							<td><h5>Subtotal</h5></td>
							<td class="text-right"><h5><strong>$<?= $totalAmount ?></strong></h5></td>
						</tr>
						<tr>
							<td>   </td>
							<td>   </td>
							<td>   </td>
							<td><h5>Estimated shipping</h5></td>
							<td class="text-right"><h5><strong>$20.00</strong></h5></td>
						</tr>
						<tr>
							<td>   </td>
							<td>   </td>
							<td>   </td>
							<td><h3>Total</h3></td>
							<td class="text-right"><h3><strong>$<?= ($totalAmount*1.13)+20 ?></strong></h3></td>
						</tr>
						<tr>
							<td>   </td>
							<td>   </td>
							<td>   </td>
							<td>
							<a type="button" href="<?= base_url(); ?>index.php?/Products" class="btn btn-default">
								<span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
							</a></td>
							<td>
							 <div id="paypal-button-container"></div>
							</td>
							
						</tr>
				
                </tbody>
            </table>
        </div>
		<?php }else{ ?>
		 <div class="col-sm-12 col-md-10 col-md-offset-1" >
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Prices</th>
                        <th class="text-center">Total</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
						<tr>
							<td>   </td>
							<td>   </td>
							<td>   </td>
							<td class="text-left"><h3><strong>your shopping cart is empty!</strong></h3></td>
						</tr>
				
				
                </tbody>
            </table>
        </div>
		
		<?php } ?>
    </div>
</div>
</div>

    <script>
        paypal.Button.render({
			


            env: 'production', // sandbox | production

            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: {
				sandbox:    'AcgvJThliMe6GquYMtVDMq2DbCn9xXN66wD__zMeKbB3N5TZOCDtlCBlrEgDaECPujHQpK_v-eedxNkP',
                production: 'AaTixkYVSlogPayoxfMncOc2fSvbKEFf4rbtM3Ewrxn8-nrxxah12rA9X3psFhjg2rlHiLH-vPdF--E4'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {

                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: '0.01', currency: 'CAD' }
                            }
                        ]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {

            // Get the payment details

            return actions.payment.get().then(function(data) {

                var shipping = data.payer.payer_info.shipping_address;

                    return actions.payment.execute().then(function() {
						
						
						window.location.href = " <?= base_url(); ?>index.php?/Shopping_cart/Add_To_Orders/"+shipping.line1+"/"+shipping.city+"/"+shipping.state+"/"+shipping.postal_code+"/"+shipping.country_code;

                    });

            });
        }

        }, '#paypal-button-container');

    </script>