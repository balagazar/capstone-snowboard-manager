<div id="body">
<div class="container" style="background-color:white;">






<?php if($Rent_inv_view){?>

<h2>Rental Inventory</h2>
	<br>
	
	<br>
	<br>
	<table style="" class="table table-condensed table-hover table-bordered">
		<thead>
			<tr>
				<th>Item id</td>
				<th>Item Name</td>
				<th>Category</td>
				<th>Price per day</td>
				<th>Item size</td>
				<th>Remove</td>
			</tr>
		</thead>
		<tbody>
			<?php foreach($rent_inv as $rent) { ?>
			 <tr>
				 <td>
						<?= $rent['rent_id']?>
				 </td>
				 
				 <td>
						<?= $rent['Name']?>
				 </td>
				 
				  <td>
					<?= $rent['Category']?>
				 </td>
				 
				 <td>
					<?= $rent['rent_price_per_day']?>
				 </td>
				
				<td>	
			 		<?= $rent['item_size']?>
				</td>
				
				
				
				<td>
					<?= form_open('Admin/delete_rent_item') ?>
						<button value="<?= $rent['id'] ?>" name="submit" type="submit"  class="glyphicon glyphicon-remove icon-size" >
						</button>
					<?= form_close() ?>
					
				
				</td>

			 </tr>
			<?php } ?>
		</tbody>
	</table>







<?php }else if($rents_view){?>
	<h2>Rental Logs</h2>
	<br>
	<br>
	<table style="" class="table table-condensed table-hover table-bordered">
		<thead>
			<tr>
				<th>Item id</td>
				<th>Item Name</td>
				<th>Category</td>
				<th>Price per day</td>
				<th>Item size</td>
				<th>Check in date</td>
				<th>Return Date</td>
				<th>Rented By</td>
			</tr>
		</thead>
		<tbody>
			<?php foreach($rents as $rent) { ?>
			 <tr>
				 <td>
						<?= $rent['rent_id']?>
				 </td>
				 
				 <td>
						<?= $rent['Name']?>
				 </td>
				 
				  <td>
					<?= $rent['Category']?>
				 </td>
				 
				 <td>
					<?= $rent['rent_price_per_day']?>
				 </td>
				
				<td>	
			 		<?= $rent['item_size']?>
				</td>
				
				<td>	
			 		<?= $rent['check_in_date']?>
				</td>
				
				
				<td>
					<?= $rent['return_date']?>
				
				</td>
				
				<td>
					<?= $rent['rented_by']?>
				
				</td>
				
				
				<td>
					<?= form_open('Admin/delete_rent') ?>
						<button value="<?= $rent['id'] ?>" name="submit" type="submit"  class="glyphicon glyphicon-remove icon-size" >
						</button>
					<?= form_close() ?>
					
				
				</td>

			 </tr>
			<?php } ?>
		</tbody>
	</table>
	
	
	
	<?php }else if($products_view){ ?>
	<h2>Products</h2>
	<br>
	<a type="button" class="btn btn-success" href="<?= base_url(); ?>index.php?/Products/Add_product_view">ADD PRODUCT</a>
	<br>
	<br>
	<table style="" class="table table-condensed table-hover table-bordered">
		<thead>
			<tr>
				<th>product id</td>
				<th>Category</td>
				<th>Product Price</td>
				<th>Product description</td>
				<th>Product Name</td>
				<th>Product Picture</td>
				<th>Actions</td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($products as $product) { ?>
			 <tr>
				 <td>
						<?= $product['product_id']?>
				 </td>
				 
				  <td>
					<?= $product['category']?>
				 </td>
				 
				 <td>
					<?= $product['product_price']?>
				 </td>
				
				<td>	
			 		<?= substr($product['product_description'], 0, 50)?>
				</td>
				
				<td>	
			 		<?= $product['product_name']?>
				</td>
				
				
				<td>
					<?= $product['product_picture']?>
				
				</td>
				
				
				<td>
					<?= form_open('Admin/delete_product') ?>
						<button value="<?= $product['product_id'] ?>" name="submit" type="submit"  class="glyphicon glyphicon-remove icon-size" >
						</button>
					<?= form_close() ?>
					
				
				</td>

			 </tr>
			<?php } ?>
		</tbody>
	</table>
	
	


<?php }else if($messages_log_view){ ?>
<h2>Messages logs</h2>

	<table style="" class="table">
		<thead>
			<tr>
				<th>Message id</td>
				<th>Message from</td>
				<th>Message to</td>
				<th>was message read</td>
				<th>Message content</td>
				<th>Message Subject</td>
				<th>Actions</td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($messages as $message) { ?>
			 <tr>
				 <td>
						<?= $message['message_id']?>
				 </td>
				 
				  <td>
					<a href="<?= base_url();?>index.php?/Other_user_profile/get_info/<?= $message['message_from']?>"  data-type="text">
						<?= $message['message_from']?>
					</a>
				 </td>
				 
				 <td>
					<a href="<?= base_url();?>index.php?/Other_user_profile/get_info/<?= $message['message_to']?>"  data-type="text">
						<?= $message['message_to']?>
					</a>
				 </td>
				
				<td>	
			 		<?= $message['message_date']?>
				</td>
				
				<td>	
			 		<?= $message['message_time']?>
				</td>
				
				
				<td>
					<?= $message['was_message_read']?>
				
				</td>
				<td>
					<?= $message['message_content']?>
				
				</td>
				
				<td>
					<?= $message['message_subject']?>
				
				</td>
				
				<td>
					<?= form_open('Admin/delete_message') ?>
						<button value="<?= $message['message_id'] ?>" name="submit" type="submit"  class="glyphicon glyphicon-remove icon-size" >
						</button>
					<?= form_close() ?>
					
				
				</td>

			 </tr>
			<?php } ?>
		</tbody>
	</table>

<?php }else if($reports_view){ ?>
<h2>Reports</h2>

	<table style="" class="table">
		<thead>
			<tr>
				<th>Offender</td>
				<th>Reported_by</td>
				<th>Report_content</td>
				<th>reported_date</td>
				<th></td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($reports as $report) { ?>
			 <tr>
				 <td>
					<a href="<?= base_url();?>index.php?/Other_user_profile/get_info/<?= $report['offender']?>"  data-type="text">
						<?= $report['offender']?>
					</a>
				 </td>
				 
				  <td>
					<a href="<?= base_url();?>index.php?/Other_user_profile/get_info/<?= $report['reported_by']?>"  data-type="text">
						<?= $report['reported_by']?>
					</a>
				 </td>
				
				<td>	
			 		<?= $report['report_content']?>
				</td>
				
				<td>	
			 		<?= $report['reported_date']?>
				</td>
				
				
				<td>
					<?= form_open('Admin/delete_report') ?>
						<button value="<?= $report['report_id'] ?>" name="submit" type="submit"  class="glyphicon glyphicon-remove icon-size" >
						</button>
					<?= form_close() ?>
					
				
				</td>

			 </tr>
			<?php } ?>
		</tbody>
	</table>

<?php }else if($orders_view){ ?>

	<h2>Current Orders</h2>
<p>
	<table id="admin_orders">
		<thead>
			<tr>
				<th>Order ID</td>
				<th>Username</td>
				<th>Shipping address</td>
				<th>City</td>
				<th>Province</td>
				<th>Country</td>
				<th>Postal Code</td>
				<th>Produce_id</td>
				<th>quantity</td>
				<th>Order status</td>
				<th></td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($orders as $order) { ?>
			 <tr>
				 <td><?= $order['order_id']?></td>
				 
				 <td>
					<a href="#"  data-type="text" class="editable editable-click editable-open" data-pk="<?= $order['order_id'] ?>" data-url="<?= base_url();?>index.php?/Admin/update/username" data-title="Username">
						<?= $order['username']?>
					</a>
				 </td>
				 
				  <td>
			 	<a href="#"  data-type="text" class="editable editable-click editable-open" data-pk="<?= $order['order_id'] ?>" data-url="<?= base_url();?>index.php?/Admin/update/shipping_address" data-title="Shipping address">
			 		<?= $order['shipping_address']?>
			 	</a>
				</td>
				
				<td>
			 	<a href="#"  data-type="text" class="editable editable-click editable-open" data-pk="<?= $order['order_id'] ?>" data-url="<?= base_url();?>index.php?/Admin/update/city" data-title="City">
			 		<?= $order['city']?>
			 	</a>
				</td>
				
				<td>
			 	<a href="#"  data-type="text" class="editable editable-click editable-open" data-pk="<?= $order['order_id'] ?>" data-url="<?= base_url();?>index.php?/Admin/update/province" data-title="Province">
			 		<?= $order['province']?>
			 	</a>
				</td>
				
				<td>
			 	<a href="#"  data-type="text" class="editable editable-click editable-open" data-pk="<?= $order['order_id'] ?>" data-url="<?= base_url();?>index.php?/Admin/update/country" data-title="Country">
			 		<?= $order['country']?>
			 	</a>
				</td>
				
				<td>
			 	<a href="#"  data-type="text" class="editable editable-click editable-open" data-pk="<?= $order['order_id'] ?>" data-url="<?= base_url();?>index.php?/Admin/update/postal_code" data-title="Postal code">
			 		<?= $order['postal_code']?>
			 	</a>
				</td>
				
				<td>
			 	<a href="#"  data-type="text" class="editable editable-click editable-open" data-pk="<?= $order['order_id'] ?>" data-url="<?= base_url();?>index.php?/Admin/update/product_id" data-title="Product id">
			 		<?= $order['product_id']?>
			 	</a>
				</td>
				
				<td>
			 	<a href="#"  data-type="text" class="editable editable-click editable-open" data-pk="<?= $order['order_id'] ?>" data-url="<?= base_url();?>index.php?/Admin/update/quantity" data-title="Quantity">
			 		<?= $order['quantity']?>
			 	</a>
				</td>
				
				<td>
			 	<a href="#"  data-type="text" class="editable editable-click editable-open" data-pk="<?= $order['order_id'] ?>" data-url="<?= base_url();?>index.php?/Admin/update/status" data-title="Status">
			 		<?= $order['status']?>
			 	</a>
				</td>
				
				<td>
					<?= form_open('Admin/delete_order') ?>
						<button value="<?= $order['order_id'] ?>" name="submit" type="submit"  class="glyphicon glyphicon-remove icon-size" >
						</button>
					<?= form_close() ?>
				
				</td>

			 </tr>
			<?php } ?>
		</tbody>
	</table>
</p>

<?php }else{ ?>

	<h2>Admin page</h2>
	<?php if($error){?>
	<div class = "error">
		<strong>
			<?=validation_errors() ?>
		</strong>
	</div>
	<?php } ?>
	<?php if($userExists){?>
		<p>
			<strong>A user with that username already exists!</strong>
		</p>
		<p>
			<strong>Hello<?php echo $username?></strong>
		</p>
	<?php } ?>
	<h3>User Table</h3>
	<table class="table table-condensed table-hover table-bordered">
	  <thead class="thead-dark">
		<tr>
		  <th scope="col">user id</th>
		  <th scope="col">Delete</th>
		  <th scope="col">Freeze/Unfreeze</th>
		  <th scope="col">Username</th>
		  <th scope="col">Password</th>
		  <th scope="col">User access</th>
		  <th scope="col">Frozen</th>
		</tr>
	  </thead>
		<tbody>
			<?php
				foreach ($listing as $row) {?>
					<tr>
					<th scope="row"><?= $row['user_id']?></th>
					 <td><a href="<?= base_url() ?>index.php?/Admin/delete/<?= $row['user_id']?>">D</a></td>
					 <td><a href="<?= base_url() ?>index.php?/Admin/freeze/<?= $row['user_id']?>">F</a></td>
					 <td><?= $row['username']?></td>
					 <td><?= $row['password']?></td>
					 <td><?= $row['accesslevel']?></td>
					 <td><?= $row['frozen']?></td>
					 </tr>

				<?php }
			?>
		</tbody>
	</table>
	<div class="text-center">
	<button id="add_user" class="btn btn-info">Add New User</button>
	</div>
	<br>
	<div id="panel" class="text-center" style="display:none;">
	<?= form_open('Admin/adduser') ?>
	<?= form_label('Username:', 'username'); ?> <br>
	<?= form_input(array('name' => 'username',
	 'id' => 'username', 'value' => set_value('username'))); ?> <br>

	<?= form_label('Password:', 'password'); ?> <br>
	<?= form_input(array('name' => 'password',
	 'id' => 'password', 'value' => set_value('password'))); ?> <br>

	 <?= form_label('Access Level:', 'accesslevel'); ?> <br>
	<?= form_input(array('name' => 'accesslevel',
	 'id' => 'accesslevel', 'value' => set_value('accesslevel'))); ?> <br>
	<br>
	<input type="submit" class="btn btn-info" value="Add"/>
	<?= form_fieldset_close(); ?>
	<?= form_close() ?>
	</div>
	<?php }?>
	
	</div>
</div>
<script> 
$(document).ready(function(){
    $("#add_user").click(function(){
        $("#panel").slideDown("slow");
    });
});
</script>