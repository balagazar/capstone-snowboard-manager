<div id="body">
<div class="container"  style="margin-top:20px;margin-bottom:20px;background-color:white;">
					<script>
						var admin_logged = false;
					</script>	
						

<div class="wrap">
    <div class="menu">
        <div class="mini-menu"><br><br><br>
            <ul>
        <li class="sub">
            <a href="<?= base_url(); ?>index.php?/Products/view_snowboards">SNOWBOARDS</a>
        </li>
        <li class="sub">
            <a href="<?= base_url(); ?>index.php?/Products/view_gloves">GLOVES</a>
        </li>
        <li class="sub">
            <a href="<?= base_url(); ?>index.php?/Products/view_boots">BOOTS</a>
        </li>
        <li class="sub">
            <a href="<?= base_url(); ?>index.php?/Products/view_helmets">HELMETS</a>
        </li>
    </ul>
        <br>
		<br>
		<br>
        
		<ul>
            <div class="header-item" >Price</div>
		<li class="sub">
            <a href="<?= base_url(); ?>index.php?/Products/high_to_low_price">High to low</a>
        </li>
		<li class="sub">
            <a href="<?= base_url(); ?>index.php?/Products/low_to_high_price">Low to high</a>
        </li>
    </ul>
	<br>
	<br>
	<?= $files ?>
		<?php if($user_access =='admin'){ ?>
			<h4>Admin:</h4>
			<a type="button" class="btn btn-success btn-md" href="<?= base_url(); ?>index.php?/Products/Add_product_view">ADD PRODUCT
					</a>
			
		<?php } ?>
	
	<br>
	<br>
	<br>
		<h5 class="text-center">Search product</h5>
    <div class="input-group">
      <input type="text" class="form-control" placeholder="Search" id="search_product" name="search_product">
      <div class="input-group-btn">
        <button class="btn btn-default" type="submit"><i style="height:9px;"class="glyphicon glyphicon-search"></i></button>
      </div>
    </div>
        </div>
	
    </div>
	
	<!-- if Admin trying to add product view-->
	<?php if ($add_product_view){ ?>
		<div class="items">
				<?= form_open_multipart('Products/Add_product') ?>
								<div class="row">
        <div class="col-md-6 col-md-offset-3">
									<legend>Adding product</legend>

                    <input type="text" name="product_name" value="" class="form-control input-lg" placeholder="product name" required  />
					<br>
					<input type="number" step="0.01" name="price" value="" class="form-control input-lg" placeholder="price $" required  />
					<br>
					<textarea class="form-control" cols="50"  name="product_description" placeholder="product description" rows="5"></textarea>
					<br>
					<label>product category</label>                    
								<select name="category" class = "form-control" required>
									<option value="snowboards">snowboard</option>
									<option value="boots">boots</option>
									<option value="gloves">gloves</option>
									<option value="helmet">helmet</option>
								</select>
						<br>
						<br>
						<input class="btn btn-primary text-center"  type="file" name="userfile" size="20" style="margin-left:45px;width:250px;"/><br>
						
						</form>
						<?php if (isset($uploaded_successfully)){?>
						<?=$uploaded_successfully?>
						<?php } ?>
						
						<button class="btn btn-success btn-block" type="submit" name="submit" id="submit"> Add product</button>
						<span id="error_message" class="text-success"></span>
						<span id="success_message" class="text-success"></span>
			<?= form_close() ?>
			
						
			 <br />
          </div>          

	</div>
					<?= form_close()?>
		</div>
		
	<?php }else if(!$Single_Item){ ?>
    <div class="items" id="products">
        
  <div class="items" id="items_searched">

	<?php foreach($products as $product) { ?>
                    <div class="item"><a href="<?= base_url(); ?>index.php?/Products/Single_Product/<?= $product['product'] ?>">
            <img src="<?= assetUrl();?><?= $product['product_picture']?>" alt="<?= $product['product_name'] ?>" class="img-item"></img>
                <div class="info">
				<div id="stars-existing" class="starrr" style="color:#e2df18;font-size:1.1em;" data-rating='<?= $product['avg_rate'] ?>'></div>
                    <h3 style="margin-top:5px;"><?= $product['product_name'] ?></h3>
					
        
    
                    <h5 style="margin-bottom:2px;"><strong>$<?= $product['product_price'] ?></strong></h5>
					<a type="button" class="btn btn-default btn-xs" href="<?= base_url(); ?>index.php?/Products/AddToCart/<?= $product['product'] ?>">
						<span class="glyphicon glyphicon-shopping-cart"></span>Add to cart
					</a>
					<?php if($user_access =='admin'){ ?>
					<script>
						admin_logged = true;
					</script>
						<a type="button" class="btn btn-danger btn-xs" href="<?= base_url(); ?>index.php?/Products/delete_product/<?= $product['product'] ?>">Delete product
								</a>
						
					<?php } ?>
                </div>
				</a>
					</div>
	<?php } ?>
    </div>
    </div>
	
	<?php }else{ ?>
		<div class="bigitems">
						<div data-price="<?= $product['product_price'] ?>" class="bigitem">
				<img src="<?= assetUrl();?><?= $product['product_picture']?>" alt="<?= $product['product_name'] ?>" class="img-item"></img>
					<div class="info">
						<h3><?= $product['product_name'] ?></h3>
						<h5><strong>$<?= $product['product_price'] ?></strong></h5>
						<a type="button" class="btn btn-default btn-xs" href="<?= base_url(); ?>index.php?/Products/AddToCart/<?= $product['product_id'] ?>">
							<span class="glyphicon glyphicon-shopping-cart"></span>Add to cart
						</a>
					</div>
						</div>
						
					<div>
						<h3>Description</h3>
						<?= $product['product_description'] ?>
					</div>
					<br>
					<br>
					
	<?php if($no_reviews == false){ ?>
		<div class="container reviews">
			<div class="row">
				<div class="col-sm-3">
					<div class="rating-block">
						<h4>Average user rating</h4>
						<h2 class="bold padding-bottom-7"><?= $rnd_average_rating ?><small>/ 5</small></h2>
						<?php for ($i = 1; $i <= 5; $i++) { ?>
							<?php if($i <= $rnd_average_rating){ ?>
									<button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
									  <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
									
								<?php }else if($i > $rnd_average_rating){ ?>
									<button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
									 <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
									</button>
								<?php  } ?>
						<?php } ?>
					</div>
				</div>
				<div class="col-sm-3 rating-breakdown">
					<h4>Rating breakdown</h4>
					<div class="pull-left">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">5 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px;">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: <?=($star_amounts['5']/$total_reviws)*100 ?>%">
								<span class="sr-only"></span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;"><?=$star_amounts['5'] ?></div>
					</div>
					<div class="pull-left">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">4 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px;">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: <?=($star_amounts['4']/$total_reviws)*100 ?>%">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;"><?=$star_amounts['4'] ?></div>
					</div>
					<div class="pull-left">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">3 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px;">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: <?=($star_amounts['3']/$total_reviws)*100 ?>%">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;"><?=$star_amounts['3'] ?></div>
					</div>
					<div class="pull-left">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">2 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px;">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: <?=($star_amounts['2']/$total_reviws)*100 ?>%">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;"><?=$star_amounts['2'] ?></div>
					</div>
					<div class="pull-left">
						<div class="pull-left" style="width:35px; line-height:1;">
							<div style="height:9px; margin:5px 0;">1 <span class="glyphicon glyphicon-star"></span></div>
						</div>
						<div class="pull-left" style="width:180px;">
							<div class="progress" style="height:9px; margin:8px 0;">
							  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: <?=($star_amounts['1']/$total_reviws)*100 ?>%">
								<span class="sr-only">80% Complete (danger)</span>
							  </div>
							</div>
						</div>
						<div class="pull-right" style="margin-left:10px;"><?=$star_amounts['1'] ?></div>
					</div>
				</div>			
			</div>			
				<div class="row">
					<div class="col-sm-7">
						<hr/>
						<div class="review-block">
						
						<?php foreach($product_reviews as $review) { ?>
							<div class="row">
								<div class="col-sm-3">
									<img src="<?= assetUrl();?>img/<?=$review['username']?>/profile_pic/thumb/profile_picture_thumb.jpg"  class="img-rounded">
									<div class="review-block-name"><a href="#"><?=$review['username']?></a></div>
									<div class="review-block-date"><?= date('F d, Y', strtotime($review['review_date'])) ?><br/></div>
								</div>
								<div class="col-sm-9">
									<div class="review-block-rate">
										<?php for ($i = 1; $i <= 5; $i++) { ?>
											<?php if($i <= $review['rating']){ ?>
													<button type="button" class="btn btn-warning btn-xs text-center" aria-label="Left Align">
													  <span class="glyphicon glyphicon-star " style="font-size:1.7em;" aria-hidden="true"></span>
													</button>
													
												<?php }else if($i > $review['rating']){ ?>
													<button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
													 <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
													</button>
												<?php  } ?>
										<?php } ?>
									</div>
									<div class="review-block-description"><?= $review['review_content'] ?></div>
								</div>
							</div>
							<hr/>
						<?php } ?>
					
						</div>
					</div>
				</div>
			
				<?php if($loggedin){ ?>
					<div class="row" >
					<div class="col-md-6">
					<div class="well well-sm">
						<div class="text-right">
							<a class="btn btn-success btn-green" href="#reviews-anchor" id="open-review-box">Leave a Review</a>
						</div>
					
						<div class="row" id="post-review-box" style="display:none;">
							<div class="col-md-12">
								<?= form_open('Products/Insert_review') ?>
									<input id="ratings-hidden" name="rating" type="hidden"> 
									<input type="hidden" name="product_id" value="<?= $product['product_id'] ?>">
									<textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5"></textarea>
					
									<div class="text-right">
										<div class="stars starrr" data-rating="0"></div>
										<a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
										<span class="glyphicon glyphicon-remove"></span>Cancel</a>
										<button class="btn btn-success btn-lg" type="submit">Save</button>
									</div>
								<?= form_close()?>
							</div>
						</div>
					</div> 
					 
					</div>
					</div>
				<?php } ?>
			</div> 
		<?php }else if($no_reviews){?>
					<div class="row">
							<div class="col-sm-9">
								<div class="review-block-rate">

												
								</div>
								<div class="review-block-description"><h4><strong>There are no reiviews on this product, please tell us what you think! </strong></h4></div>
								<br>

							<?php if(!$loggedin){ ?>
								<h4><strong>Please log in to leave a review</strong></h4>
							<?php } ?>
								<br>
							
							</div>
							
					</div>
		<?php if($loggedin){ ?>
					<div class="row" >
				<div class="col-md-10">
				<div class="well well-sm">
					<div class="text-right">
						<a class="btn btn-success btn-green" href="#reviews-anchor" id="open-review-box">Leave a Review</a>
					</div>
				
					<div class="row" id="post-review-box" style="display:none;">
						<div class="col-md-12">
							<?= form_open('Products/Insert_review') ?>
								<input id="ratings-hidden" name="rating" type="hidden"> 
								<input type="hidden" name="product_id" value="<?= $product['product_id'] ?>">
								<textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5"></textarea>
				
								<div class="text-right">
									<div class="stars starrr" data-rating="0"></div>
									<a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
									<span class="glyphicon glyphicon-remove"></span>Cancel</a>
									<button class="btn btn-success btn-lg" type="submit">Save</button>
								</div>
							<?= form_close()?>
						</div>
					</div>
				</div> 
				 
				</div>
			</div>
			<?php } ?>		
				
		<?php } ?>			
		</div>
	<?php } ?>
</div>






    <!--Menu-->
	<script>

(function(e){var t,o={className:"autosizejs",append:"",callback:!1,resizeDelay:10},i='<textarea tabindex="-1" style="position:absolute; top:-999px; left:0; right:auto; bottom:auto; border:0; padding: 0; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden; transition:none; -webkit-transition:none; -moz-transition:none;"/>',n=["fontFamily","fontSize","fontWeight","fontStyle","letterSpacing","textTransform","wordSpacing","textIndent"],s=e(i).data("autosize",!0)[0];s.style.lineHeight="99px","99px"===e(s).css("lineHeight")&&n.push("lineHeight"),s.style.lineHeight="",e.fn.autosize=function(i){return this.length?(i=e.extend({},o,i||{}),s.parentNode!==document.body&&e(document.body).append(s),this.each(function(){function o(){var t,o;"getComputedStyle"in window?(t=window.getComputedStyle(u,null),o=u.getBoundingClientRect().width,e.each(["paddingLeft","paddingRight","borderLeftWidth","borderRightWidth"],function(e,i){o-=parseInt(t[i],10)}),s.style.width=o+"px"):s.style.width=Math.max(p.width(),0)+"px"}function a(){var a={};if(t=u,s.className=i.className,d=parseInt(p.css("maxHeight"),10),e.each(n,function(e,t){a[t]=p.css(t)}),e(s).css(a),o(),window.chrome){var r=u.style.width;u.style.width="0px",u.offsetWidth,u.style.width=r}}function r(){var e,n;t!==u?a():o(),s.value=u.value+i.append,s.style.overflowY=u.style.overflowY,n=parseInt(u.style.height,10),s.scrollTop=0,s.scrollTop=9e4,e=s.scrollTop,d&&e>d?(u.style.overflowY="scroll",e=d):(u.style.overflowY="hidden",c>e&&(e=c)),e+=w,n!==e&&(u.style.height=e+"px",f&&i.callback.call(u,u))}function l(){clearTimeout(h),h=setTimeout(function(){var e=p.width();e!==g&&(g=e,r())},parseInt(i.resizeDelay,10))}var d,c,h,u=this,p=e(u),w=0,f=e.isFunction(i.callback),z={height:u.style.height,overflow:u.style.overflow,overflowY:u.style.overflowY,wordWrap:u.style.wordWrap,resize:u.style.resize},g=p.width();p.data("autosize")||(p.data("autosize",!0),("border-box"===p.css("box-sizing")||"border-box"===p.css("-moz-box-sizing")||"border-box"===p.css("-webkit-box-sizing"))&&(w=p.outerHeight()-p.height()),c=Math.max(parseInt(p.css("minHeight"),10)-w||0,p.height()),p.css({overflow:"hidden",overflowY:"hidden",wordWrap:"break-word",resize:"none"===p.css("resize")||"vertical"===p.css("resize")?"none":"horizontal"}),"onpropertychange"in u?"oninput"in u?p.on("input.autosize keyup.autosize",r):p.on("propertychange.autosize",function(){"value"===event.propertyName&&r()}):p.on("input.autosize",r),i.resizeDelay!==!1&&e(window).on("resize.autosize",l),p.on("autosize.resize",r),p.on("autosize.resizeIncludeStyle",function(){t=null,r()}),p.on("autosize.destroy",function(){t=null,clearTimeout(h),e(window).off("resize",l),p.off("autosize").off(".autosize").css(z).removeData("autosize")}),r())})):this}})(window.jQuery||window.$);

var __slice=[].slice;(function(e,t){var n;n=function(){function t(t,n){var r,i,s,o=this;this.options=e.extend({},this.defaults,n);this.$el=t;s=this.defaults;for(r in s){i=s[r];if(this.$el.data(r)!=null){this.options[r]=this.$el.data(r)}}this.createStars();this.syncRating();this.$el.on("mouseover.starrr","span",function(e){return o.syncRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("mouseout.starrr",function(){return o.syncRating()});this.$el.on("click.starrr","span",function(e){return o.setRating(o.$el.find("span").index(e.currentTarget)+1)});this.$el.on("starrr:change",this.options.change)}t.prototype.defaults={rating:void 0,numStars:5,change:function(e,t){}};t.prototype.createStars=function(){var e,t,n;n=[];for(e=1,t=this.options.numStars;1<=t?e<=t:e>=t;1<=t?e++:e--){n.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"))}return n};t.prototype.setRating=function(e){if(this.options.rating===e){e=void 0}this.options.rating=e;this.syncRating();return this.$el.trigger("starrr:change",e)};t.prototype.syncRating=function(e){var t,n,r,i;e||(e=this.options.rating);if(e){for(t=n=0,i=e-1;0<=i?n<=i:n>=i;t=0<=i?++n:--n){this.$el.find("span").eq(t).removeClass("glyphicon-star-empty").addClass("glyphicon-star")}}if(e&&e<5){for(t=r=e;e<=4?r<=4:r>=4;t=e<=4?++r:--r){this.$el.find("span").eq(t).removeClass("glyphicon-star").addClass("glyphicon-star-empty")}}if(!e){return this.$el.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty")}};return t}();return e.fn.extend({starrr:function(){var t,r;r=arguments[0],t=2<=arguments.length?__slice.call(arguments,1):[];return this.each(function(){var i;i=e(this).data("star-rating");if(!i){e(this).data("star-rating",i=new n(e(this),r))}if(typeof r==="string"){return i[r].apply(i,t)}})}})})(window.jQuery,window);$(function(){return $(".starrr").starrr()})

$(function(){

  $('#new-review').autosize({append: "\n"});

  var reviewBox = $('#post-review-box');
  var newReview = $('#new-review');
  var openReviewBtn = $('#open-review-box');
  var closeReviewBtn = $('#close-review-box');
  var ratingsField = $('#ratings-hidden');

  openReviewBtn.click(function(e)
  {
    reviewBox.slideDown(400, function()
      {
        $('#new-review').trigger('autosize.resize');
        newReview.focus();
      });
    openReviewBtn.fadeOut(100);
    closeReviewBtn.show();
  });

  closeReviewBtn.click(function(e)
  {
    e.preventDefault();
    reviewBox.slideUp(300, function()
      {
        newReview.focus();
        openReviewBtn.fadeIn(200);
      });
    closeReviewBtn.hide();
    
  });

  $('.starrr').on('starrr:change', function(e, value){
    ratingsField.val(value);
  });
});


	$(document).ready(function() {
	
$("#search_product").keyup(function() {
  		$.ajax({
  			dataType: "json",
  			url:BASE_URL+"index.php?/Products/find_product/"+$("#search_product").val(),
  			success:function(data){
  				var buildTable = "";
  				for (var i = 0; i < data.length; i++) {
					
				buildTable += "<div data-price="+data[i].product_price+" class='item'><a href="+BASE_URL+"index.php?/Products/Single_Product/"+data[i].product_id+ ">";
				  buildTable += "<img src="+assetUrl+""+data[i].product_picture+" alt="+data[i].product_name+" class='img-item'></img>";
                 buildTable += "<div class='info'>";
                buildTable += "<h3>"+data[i].product_name+"</h3>";

                buildTable +="<h5><strong>$"+data[i].product_price+"</strong></h5>";
				buildTable +="<a type='button' class='btn btn-default btn-xs' href="+BASE_URL+"index.php?/Products/AddToCart/"+data[i].product_id+">";
				buildTable +="<span class='glyphicon glyphicon-shopping-cart'></span>Add to cart";
				buildTable +=	"</a>"
				
				if(admin_logged==true){
					buildTable +="<a type='button' class='btn btn-danger btn-xs' href="+BASE_URL+"index.php?/Products/delete_product/"+data[i].product_id+">Delete product</a>";
				}
				
                buildTable +="</div></a></div>";
				
						
						

				}
				$('#items_searched').html(buildTable);
			},
  		});
		
		});

});
</script>
</div>
</div>