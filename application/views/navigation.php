
<?php 
$username = $this->userauth->getUsername();
	$sql = "SELECT count(*) as count FROM shopping_cart WHERE username = '$username'" ;
    $query = $this->db->query($sql);
	$result = $query->result_array()[0];
	
	$sql = "SELECT count(*) as count FROM message WHERE message_to = '$username' and was_message_read = 'N'" ;
    $query = $this->db->query($sql);
	$message_result = $query->result_array()[0];
	
?>

<a href="<?= base_url(); ?>index.php?/Home">
<!--<img  class="snowmanager_logo" src="<?= assetUrl();?>img/Snowboar-logo.png">-->
</a>
<!--When used is logged in  -->
<?php if ($loggedin) { ?>

<nav id="navbar" class="navbar navbar-inverse" style="margin-bottom:0;">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?= base_url(); ?>index.php?/Home">Snowboard Manager</a>
    </div>
    <ul class="nav navbar-nav">
      <li class = "<?=isset($home)?'active':'' ?>"><a href="<?= base_url(); ?>index.php?/Home">Home</a></li>
      <li class = "<?=isset($rent_page)?'active':'' ?>"><a href="<?= base_url(); ?>index.php/Rent">Rent</a></li>
      <li class = "<?=isset($products)?'active':'' ?>"><a href="<?= base_url(); ?>index.php?/Products">Products</a></li>
	  <li class = "<?=isset($contacts)?'active':'' ?>"><a href="<?= base_url(); ?>index.php?/Contact">Contact</a></li>
	   <li class="navbar-form navbar-left" style="margin-top:10px;border-left: 2px solid black;">
       <div class="form-group">
        <input type="text" id="search_me_up" class="form-control dropdown-toggle" data-toggle="dropdown" placeholder="Search" >
      </div>
	  
    </ul>
	
    <ul class="nav navbar-nav navbar-right">
	
		<?php 
		$username=$this->userauth->getUsername();
		if(file_exists('./application/assets/img/'.$username.'/profile_pic/thumb/profile_picture_thumb.jpg')){ ?>
			<img src="<?= assetUrl();?>img/<?=$username?>/profile_pic/thumb/profile_picture_thumb.jpg" style="float:left;margin-top:2px;margin-right:10px;border-radius: 10px;border: 3px solid #BADA55;">
		<?php } ?>
		
		
		<li><a href="<?= base_url(); ?>index.php?/Shopping_cart"><span id="logos" class="glyphicon glyphicon-shopping-cart icon-size"></span><?php if($result['count']>0){ ?><span class="badge badge-notify"><?=$result['count']?></span><?php } ?></a></li>
		<li><a href="<?= base_url(); ?>index.php?/Message"><span  id="logos" class="glyphicon glyphicon-envelope icon-size"></span><?php if($message_result['count']>0){ ?><span class="badge badge-notify"><?=$message_result['count']?></span><?php } ?></a></li>
	   <li class="dropdown">
        <a class="dropdown-toggle glyphicon glyphicon-cog icon-size"  data-toggle="dropdown" href="#">
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
			<?php if($user_access =='admin'){ ?>
			<li><a href="<?= base_url(); ?>index.php?/Admin"><span class="glyphicon glyphicon-globe"></span>Admin/Users</a></li>
			<li><a href="<?= base_url(); ?>index.php?/Admin/products_view"><span class="glyphicon glyphicon-shopping-cart"></span>Products</a></li>
			<li><a href="<?= base_url(); ?>index.php?/Admin/messaging_log_view"><span class="glyphicon glyphicon-envelope"></span>Messaging Logs</a></li>			
			<li><a href="<?= base_url(); ?>index.php?/Admin/Orders_view"><span class="glyphicon glyphicon-th-list"></span>Current orders</a></li>
			<li><a href="<?= base_url(); ?>index.php?/Admin/Rent_view"><span class="glyphicon glyphicon-tasks"></span>Rental Logs</a></li>	
			<li><a href="<?= base_url(); ?>index.php?/Admin/Rent_inv_view"><span class="glyphicon glyphicon-tags"></span>Rental Inventory</a></li>					
			<li><a href="<?= base_url(); ?>index.php?/Admin/reports_view"><span class="glyphicon glyphicon-warning-sign"></span>Reports</a></li>
			
			<?php } ?>
			<li><a href="<?= base_url(); ?>index.php?/Orders"><span class="glyphicon glyphicon-list-alt"></span> My orders</a></li>
			<li><a href="<?= base_url(); ?>index.php?/User_Profile"><span class="glyphicon glyphicon-user"></span> User Profile</a></li>
			<li><a href="<?= base_url(); ?>index.php?/Login/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
        </ul>
      </li>
	  
    </ul>
  </div>
</nav>
<div id="search_bar_div" class="">
	<ul id="search_bar_ul" class="text-center">
	</ul>
</div>
<!--When used is not loggedin  -->
 <?php } else { ?>
   <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Snowboard Manager</a>
    </div>
    <ul class="nav navbar-nav">
      <li class = "<?=isset($home)?'active':'' ?>"><a href="<?= base_url(); ?>index.php?/Home">Home</a></li>
      <li class = "<?=isset($rent_page)?'active':'' ?>"><a href="<?= base_url(); ?>index.php?/Rent">Rent</a></li>
      <li class = "<?=isset($products)?'active':'' ?>"><a href="<?= base_url(); ?>index.php?/Products">Products</a></li>
	  <li class = "<?=isset($contacts)?'active':'' ?>"><a href="<?= base_url(); ?>index.php?/Contact">Contact</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?= base_url(); ?>index.php?/New_user"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
      <li><a href="<?= base_url(); ?>index.php?/Login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
    </ul>
  </div>
</nav>

 <?php } ?>	
