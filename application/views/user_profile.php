<div id="body">
<div class="container">


        <div class="col-xs-12 col-sm-5 col-md-12">
            <div class="well well-sm">
                <div class="row top-buffer">
                    <div class="col-sm-6 col-md-4 text-center" style="margin-top:50px;">
                       
						<h3><?php echo $username?></h2>
						<?php 
						$username=$this->userauth->getUsername();
						if(file_exists('./application/assets/img/'.$username.'/profile_pic/profile_picture.jpg')){ ?>
							<img src="<?= assetUrl();?>img/<?=$username?>/profile_pic/profile_picture.jpg">
						<?php }else{?>
							<img src="<?= assetUrl();?>img/default_profile.jpg" ;>	
						<?php } ?>

						<?php echo form_open_multipart('User_Profile/do_upload');?>
						<br>

							<input class="btn btn-primary"  type="file" name="userfile" size="20" style="margin-left:45px;width:250px;"/><br>
						<div class="btn-group">
							<button class="btn btn-primary"  href="<?= base_url(); ?>index.php?/User_Profile/DeletePhoto">Delete Photo</button>
							<input class="btn btn-primary" type="submit" value="upload"  />
						</div>
						<br>


						</form>
						<?php if (isset($uploaded_successfully)){?>
						<?=$uploaded_successfully?>
						<?php } ?>

                    </div>
					
					<!--
						If the user select the change password button he is redirected to the password change form.
						In this form the user must follow rules in order to change the password
					-->
						<?php if($PassView==true){?>
						
							
							
							<?php if($error){?>
							<div class = "error">
								<strong>
									<?=validation_errors() ?>
								</strong>
							</div>
							<?php } ?>
								<div class="col-sm-6 col-md-3">
								<h3>Change Password</h3>
								<hr/>
								<?= form_open('User_Profile/ChangePassWord') ?>
									<div class="form-group">
										<?= form_label('Enter Current Password:', 'username'); ?> <br>
										<?= form_input(array('type' => 'password','class' => 'form-control','name' => 'CurrentPass')); ?>
									</div>
									 <div class="form-group">
										<?= form_label('New Password:', 'NewPass'); ?> <br>
										<?= form_input(array('type' => 'password','class' => 'form-control','name' => 'NewPass')); ?> 
									</div>
									 <div class="form-group">
										<?= form_label('Confirm Passowrd:', 'ConfirmNewPass'); ?> <br>
										<?= form_input(array('type' => 'password','class' => 'form-control', 'name' => 'ConfirmNewPass')); ?> 
									</div>
									<?= form_submit(array('data'=>'ChangePass','value'=>'Change Password','class' => 'btn btn-primary btn-sm center-block')); ?>
								<?= form_close() ?>
							
						
					 </div>
					 
						<!--
							This is the original view the user should see.
							In this view the user will be able to see all user info and be able to chage it.
						-->
						
					<?php }else if($PassView==false & $SecurityView==false){ ?>
						<div class="col-sm-6 col-md-4" id="noText">
							<?php if($success){?>
								
									<div class="alert alert-success" id="success_message">
										<h4><strong>Success!</strong> The password was succesfully changed.!</h4>
									</div>
								
								<?php } ?>
						
							<?= form_open('User_Profile/SubmitChanges') ?>
								<div class="form-group">
									<label for="email"><small>Email address:</small></label>
									<input name="email" type="email" class="form-control" value="<?=$result['email']?>" aria-describedby="emailHelp" placeholder="Enter email">
								
								
								<br>
									<label for="exampleInputEmail1"><small>Address:</small></label>
									<input name="address" type="text" class="form-control" value="<?=$result['address']?>"id="exampleInputEmail1" placeholder="Address">
								<br>
								
								<label for="exampleInputEmail1"><small>Country:</small></label>
									<input name="country" type="text" class="form-control" id="exampleInputEmail1" value="<?=$result['country']?>" placeholder="Country">
								<br>
								
								<label for="exampleInputEmail1"><small>Snowboard Type:</small></label>
									<input name="snowboardType" type="text" class="form-control" id="exampleInputEmail1" value="<?=$result['snowboardType']?>" placeholder="Snowboard type">
								
								<br>
							
								  <label for="example-date-input" class="col-2 col-form-label"><small>Birthday:</small></label>
								  
									<input name="birthday" class="form-control" type="date" value="<?=$result['birthday']?>" id="example-date-input">
									
								<br>
									 <label for="exampleTextarea"><small>About me:</small></label>
									<textarea name="about" class="form-control"  id="exampleTextarea" rows="3"><?=$result['about']?></textarea>
								</div>	
								
								<button class="btn btn-info" type="submit">Submit changes</button>
								<br>
								<br>	
								<a class="btn btn-info"  href="<?= base_url(); ?>index.php?/User_Profile/ChangePassView">Change Passowrd</a>
								<br>
								<br>
								<a class="btn btn-info"  href="<?= base_url(); ?>index.php?/User_Profile/ChangeSecurityView">Update Security Questions</a>
								
							<?= form_close() ?>								
						</div>
						<!--
							Change secuirty questions View
							In this view the user will be able to change the security questions and answers
						-->
						<?php }else if($SecurityView==true) {?>
						<div class="col-sm-6 col-md-4" id="noText">
							<h4>
							<?= form_open('User_Profile/SecurityQuestionChange') ?>
								<div class="form-group">
									<label for="email"><small>Question 1:</small></label>
									<input name="question_1" type="text" class="form-control" value="<?=$result['security_question_1']?>" placeholder="Security Question">

									<label for="exampleInputEmail1"><small>Answer:</small></label>
									<input name="answer_1" type="text" class="form-control" value="<?=$result['security_answer_1']?>" placeholder="Security Answer">
								<br>
								
									<label for="email"><small>Question 2:</small></label>
									<input name="question_2" type="text" class="form-control" value="<?=$result['security_question_2']?>" placeholder="Security Question">

									<label for="exampleInputEmail1"><small>Answer:</small></label>
									<input name="answer_2" type="text" class="form-control" value="<?=$result['security_answer_2']?>" placeholder="Security Answer">
								<br>
								<br>

								<button class="btn btn-info" type="submit">Submit changes</button>
								</div>
							<?= form_close() ?>								
						</div>
					<?php } ?>
						

                </div>
            </div>
        </div>
</div>
</div>