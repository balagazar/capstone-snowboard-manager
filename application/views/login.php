<br>
<?php if(isset($msg)) {  ?>
<div class="container">
<div class="row" id="security_view" >
<div class="col-md-6 col-md-offset-3">
	<div class="alert alert-danger" id="success_message">
	  <strong><?= $msg ?></strong>
	</div>
</div>
</div>
</div>
<?php } ?>


<?php if($User_Created) {  ?>
<div class="container">
<div class="row" id="security_view" >
<div class="col-md-6 col-md-offset-3">
	<div class="alert alert-success" id="success_message">
	  <strong>Success!</strong> A user was created!
	</div>
</div>
</div>
</div>
<?php } ?>

<?php if($Pass_changed_successfully) {  ?>
<div class="container">
<div class="row" id="security_view" >
<div class="col-md-6 col-md-offset-3">
	<div class="alert alert-success" id="success_message">
	  <strong>Success!</strong> Password was changed successfuly
	</div>
</div>
</div>
</div>
<?php } ?>


<!-- Find User name view in order to reset password  -->

<?php if($forgot_pass_view==true && $DisplayQuestions==false){ ?>
<div class="container text-center"  style="margin-top:200px;height:330px;">
<div id="div1"  class="col-lg-4 col-lg-offset-4">
		<?php if($userNotExists == true){ ?>
						<div class="alert alert-danger" >
							<strong>Error!</strong> The username <?= $username ?> does not exist! 
						</div>	
		<?php } ?>
			<div class="row">
			<?= form_open('Login/Forgot_pass_get_user_name') ?>
								<div class="form-group">
									<input name="username" type="text" class="form-control" value="" placeholder="Username">
								<br>
								<button class="btn btn-info" type="submit">Submit</button>
								</div>
			</div>
			<?= form_close() ?>	
</div>
</div>
<?php }  ?>


<!-- The Original login view  -->

<?php if($forgot_pass_view==false && $DisplayQuestions==false && $ChangePassNow==false) {  ?>
<div class="container" style="margin-bottom:12.5%;margin-top:6%;">
<div class="col-md-6 col-md-offset-3">
	<div class="text-center">
		<h2>Login</h2>
		<br>
		<?= form_open("Login/loginuser") ?>
		<div class="input-group" style="text-align:center;">
		<span class="input-group-addon" ><i  class="glyphicon glyphicon-user"></i></span>
		<?= form_input(array('name' => 'username',
		 'id' => 'username',
		 'type'=>'text',
		 'class'=>'form-control input-lg',
		 'placeholder'=>'Username',
		 'value' => set_value('username',"") )); ?>
		</div>
		<br/>
		<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			<?= form_input(array('name' => 'password',
			 'id' => 'password',
			 'type'=>'password',
			 'class'=>'form-control input-lg',
			 'placeholder'=>'Password',
			 'value' => set_value('password',"") )); ?> 
		</div>
		<br>
		<a href="<?= base_url(); ?>index.php?/Login/Forgot_pass">Forgot Password?</a>
		<br/>
		<?= form_submit(array('name'=>'loginsubmit', 'value'=>'Login','class'=>'btn btn-lg btn-primary btn-block','style'=>'margin-top:20px;')); ?>

		<?= form_fieldset_close(); ?>
		<?= form_close() ?>
</div>
	</div>
</div>
</div>
<?php } ?>



<!-- Displaying security questions after username was found  -->

<?php if($DisplayQuestions==true && $ChangePassNow==false){ ?>
<?php if($security_questions_wrong){ ?>
	<div class="container">
	<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="alert alert-danger" id="success_message">
		  <strong>Error!</strong> Security questions are incorrect! Try again!
		</div>
	</div>
	</div>
	</div>

<?php } ?>
<div class="container" style="margin-bottom:50px;height:430px;">
			<div class="row" id="security_view" >
			<div class="col-md-6 col-md-offset-3">
           	<?= form_open('Login/Forgot_pass_reset_pass',$attributes) ?>
				<legend>Security Questions</legend>
				<div class="row">
					<label>Question 1</label>
					<span name="question_1" class = "form-control input-lg"> <?= $question_1 ?></span>                        
					<label>Answer 1</label>
					<input name="answer_1" type="text" class = "form-control input-lg" required></input>
					<label>Question 2</label>
					<span name="question_2" class = "form-control input-lg"> <?= $question_2 ?></span>
					<label>Answer 2</label>
					<input name="answer_2" type="text" class = "form-control input-lg" required></input>		
					   <br />
					<input type="hidden" name="username" value="<?= $username ?>" />
					<button class="btn btn-primary btn-block" type="submit" name="submit" id="submit">Reset Passowrd</button>
						<br />
						<br />
				</div>		
			</form>
			</div>
			</div>
</div>

<?php } ?>

<!--Asks the user what password to change to. -->
<?php if($ChangePassNow==true){ ?>
	
	<?php if($Password_not_match){ ?>
	<div class="container">
	<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<div class="alert alert-danger" id="success_message">
		  <strong>Error!</strong> Password does not match! Try again!
		</div>
	</div>
	</div>
	</div>

<?php } ?>
<div class="container" style="margin-bottom:50px;height:430px;">
<div class="row" id="security_view" >
<div class="col-md-6 col-md-offset-3">
	<br>
	<br>
	<br>
	<br>
	<?= form_open('Login/Password_Reset_now') ?>
	<input type="password" name="password" value="" class="form-control input-lg" placeholder="New Password" required  />
	<br/>
	<input type="password" name="ConfirmNewPass" value="" class="form-control input-lg" placeholder="Confirm  New Password" required  /> 
	<input type="hidden" name="username" value="<?= $username ?>" />
	<br>
	<button class="btn btn-primary btn-block" type="submit" name="submit" id="submit">Reset Passowrd</button>
	<?= form_close() ?>	
	
</div>
</div>
</div>
<?php }  ?>
