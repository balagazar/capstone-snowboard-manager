</body>

<footer id="footer" class="navbar navbar-inverse">
<div class="container">
	<ul class="Sitemap">
	
		<li>
			<ul>
				<li style="color:orange;"><strong>Sitemap</strong></li>
				<li><a href="<?= base_url(); ?>index.php?/Home">Homepage</a></li>
				<li><a href="<?= base_url(); ?>index.php?/Rent">Rent</a></li>
				<li><a href="<?= base_url(); ?>index.php?/Contact">Contact</a></li>
				<li><a href="<?= base_url(); ?>index.php?/Products">Products</a></li>
			</ul>
		</li>
		
		<li>
			<ul class="Account">
				<li style="color:orange;">Account</li>
				<li><a href="<?= base_url(); ?>index.php?/New_user">Create an account</a></li>
				<li><a href="<?= base_url(); ?>index.php?/Login/Forgot_pass">Login issues?</a></li>
			</ul>
		</li>
		
		<li>
			<ul class="Products">
				<li style="color:orange;"><strong>Products</strong></li>
				<li><a href="<?= base_url(); ?>index.php?/Products/view_snowboards">Snowboards</a></li>
				<li><a href="<?= base_url(); ?>index.php?/Products/view_helmets">Helmets</a></li>
				<li><a href="<?= base_url(); ?>index.php?/Products/view_boots">Boots</a></li>
				<li><a href="<?= base_url(); ?>index.php?/Products/view_gloves">Gloves</a></li>
			</ul>
		</li>
		
		<li>
			<ul class="Support">
				<li style="color:orange;"><strong>Support</strong></li>
				<li><a href="<?= base_url(); ?>index.php?/Contact">Contact us</a></li>
			</ul>
		</li>
		
		<li>
			<ul class="Legal">
				<li style="color:orange;"><strong>Legal</strong></li>
				<li><a href="<?= base_url(); ?>index.php?/User_agreement">User agreement</a></li>
				<li><a href="<?= base_url(); ?>index.php?/Term_of_use">Terms of Use</a></li>
				<li><a href="<?= base_url(); ?>index.php?/Privacy_policy">Privacy Policy</a></li>
			</ul>
		</li>
	</ul>
	
	<ul class="social">
		<a href="https://www.instagram.com/snowboardmanager/?hl=en"><li class="instagram" ><img src="<?= assetUrl();?>img/instagram-icon.png"></li></a>
		<a href="https://twitter.com/snowboardmanag2"><li class="twitter" ><img src="<?= assetUrl();?>img/twitter-icon.png"></li></a>
		<a href="https://www.facebook.com/profile.php?id=100019187320732"><li class="facebook"><img src="<?= assetUrl();?>img/facebook-icon.png"></li></a>
		<a href="https://www.youtube.com/channel/UCQ1jw4Ug8IhVAHmauLsPaQA"><li class="youtube"><img src="<?= assetUrl();?>img/youtube-icon.png"></li></a>
	</ul>
	<br>

	
	 <p>Copyright 2017</p>
	 </div>
</footer>


</div>
</body>
</html>