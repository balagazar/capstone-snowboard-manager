
<div id="body" >
	<div class="container" style="background-color:white;margin-top:50px;">
		<!-- The Modal -->
<div id="summery_modal" class="modal">

    <!-- Modal content -->
  <div class="modal-content" >
	<div style="height:250px;">
		<div class="modal-header-success text-center">
		  <span class="close"><span class="glyphicon glyphicon-remove"></span></span>
		  <h2>Rental summery</h2>
		</div>
		<div class="modal-body">
		   <table class="table text-left table-condensed table-hover table-bordered">
		<thead>
		  <tr class="active">
			<th class='text-center'>Item</th>
			<th class='text-center'>Size</th>
			<th class='text-center'>price per day</th>
		  </tr>
		</thead>
		<tbody id="table_body">

		</tbody>
		</table>
		
			
		</div>
	</div>
	<div class="modal-footer-check-in">
    <p class="text-center" style="font-size:1.5em;color:black;"><strong>Rental days: <span id="total_days"></span>      </strong><strong>Total: <span id="total_price"></span></strong></p>
		<div id="paypal-button-container" class="text-right"></div>
  </div>
  </div>

</div>
		<section id="RentHeader">
			<h1>Rent</h1>
			This is the rental page. you can select whici items you are interested to rent and what dates will you be renting them. 
			<br>If items are not returned by the expected date the visa holder will get a fine to the full value of the rental eqipment. Only users are allowed to rent. 
		</section>
		<br>
		<form name="myForm" id="myForm" action="" method="post" class="text-center">
				<table class="table table-striped text-center" style="background-color:white;">
					<tr><th class='text-center'>Item</th><th class='text-center'>Price</th><th class='text-center'>Size</th></tr>
					<tr id="helmets_row"><td>Helmet</td><td>$5.00/per day</td><td><select name="helmets" id="helmets"><?php foreach($Helmet_Size_options as $key => $value) {?><option onclick="check_availability(<?=$key?>,<?=$value?>)" value="<?=$value?>"><?= $key ?></option><?php } ?></select><span id="helmets_glyphicon"></span></td></tr>
					<tr id="snowboards_row"><td>Snowboard</td><td>$20.00/per day</td><td><select name="snowboards" id="snowboards"><?php foreach($Snowboard_Size_Options as $key => $value) {?><option value="<?=$value?>"><?= $key ?></option><?php } ?></select><span id="snowboards_glyphicon"></span></td></tr>
					<tr id="boots_row"><td>Boots</td><td>$5.00/per day</td><td><select name="boots" id="boots"><?php foreach($Boots_Size_Options as $key => $value) {?><option value="<?=$value?>"><?= $key ?></option><?php } ?></select></td><span id="boots_glyphicon"></span></tr>
					<tr id="gloves_row"><td>Gloves</td><td>$2.00/per day</td><td><select name="gloves" id="gloves"><?php foreach($Gloves_Size_options as $key => $value) {?><option value="<?=$value?>"><?= $key ?></option><?php } ?></select><span id="gloves_glyphicon"></span></td></tr>
				</table>
				<br>
				<?php if (!$loggedin) { ?>
					<p>Please login to rent items</p>
				<?php }else{ ?>
					<p class="text-center">Check out Date: <input name="Date_1" id="datepicker" required >Return Date: <input name="Date_2" id="datepicker2" required>  </p>
					  <br>
					<input class="btn btn-default" type="submit" name="submit" value="Rent"/>
					<br>
					<br>
				<?php }?>
			
				<?php if($have_rented_items){ ?>
				<br>
				<hr style="
    display: block;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    margin-left: auto;
    margin-right: auto;
    border-style: inset;
    border-width: 1px;
 ">
				<h3>Currently rented items</h3>
				<br>
		<table class="table text-left table-bordered table-hover table-condensed">
		  <thead class="thead-dark ">
			<tr>
			  <th scope="col">Item name</th>
			  <th scope="col">Category</th>
			  <th scope="col">Item size</th>
			  <th scope="col">Check in date</th>
			  <th scope="col">Return date</th>
			</tr>
		  </thead>
			<tbody>
				<?php
					foreach ($rented_items as $rent_item) {?>
						<tr>
						 <td><?= $rent_item['Name']?></td>
						 <td><?= $rent_item['Category']?></td>
						 <td><?= $rent_item['item_size']?></td>
						 <td><?= $rent_item['check_in_date']?></td>
						 <td><?= $rent_item['return_date']?></td>
						 </tr>

					<?php }
				?>
			</tbody>
		</table>
			
		<?php } ?>
		
					
			
		</form>
		<br><br>
		
		
		


<!-- The Modal -->
<div id="item_missing_model" class="modal2">

    <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header text-center">
      <span class="close"><span class="glyphicon glyphicon-remove"></span></span>
      <h2>Sorry for inconvenience,</h2>
    </div>
    <div class="modal-body text-center">
      <p><strong>but those items are unavailable at the moment. Please change check in date for the following items</strong></p>
	  <ul id="myList">
	  </ul>
    </div>
  </div>

</div>

<div id="modal_success" class="modal_success">

    <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-header_success text-center">
      <h2>Success!!</h2>
    </div>
    <div class="modal-body text-center">
      <p><strong>You have successfully rented the requiested items.</strong></p>
    </div>
  </div>

</div>

<script>
var modal = document.getElementById('myModal');


// Get the <span> element that closes the modal
var close = document.getElementsByClassName("close")[0];
var modal_success = document.getElementById('modal_success');




// Get the <span> element that closes the modal
var close_item_missing_modal = document.getElementsByClassName("close_item_missing")[0];

var modal2_close_button = document.getElementsByClassName("close")[1];


var firstDate = "";
var secondDate = "";

var items = [""];
var sizes = [""];
var check_in_date = [""];
var return_date = [""];

// Get the modal
var success_items = [""];
var seccess_sizes = [""];
var success_item_id = [""];
var success_item_price = [""];
var total_items_added;


$(document).ready(function(){
    $("#myForm").submit(function(e) {
total_items_added = 0;
		e.preventDefault();
		var total_price =0;
		// Get the modal
items = [""];
sizes = [""];
check_in_date = [""];
return_date = [""];

// Get the modal
success_items = [""];
seccess_sizes = [""];
success_item_id = [""];
success_item_price = [""];
		
var items_not_selected = 0;
// When the user clicks the button, open the modal 
	

		
			var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
			
			firstDate = new Date(document.getElementById("datepicker").value);
			secondDate = new Date(document.getElementById("datepicker2").value);

			var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
			
			firstDate = formatDate(firstDate);

			secondDate = formatDate(secondDate);
			
			var spot_available = 0;
				
				var inputDate = new Date(document.getElementById("datepicker").value);
				var inputDate2 = new Date(document.getElementById("datepicker2").value);
				var todaysDate = new Date();

				if(inputDate.setHours(0,0,0,0) < todaysDate.setHours(0,0,0,0)){
					alert("Check out date can't be in the past");
				}else if(firstDate > secondDate){
					alert("Dates are mixed up, Check out date must be before Return date ");
				}else if(inputDate2.setHours(0,0,0,0) < todaysDate.setHours(0,0,0,0))
				{
					alert("Return date can not be in the past!");
				}else{
			
			
			
		for (let i=0; i<4; i++) 
		{

			var item_id = document.forms["myForm"][i].id; 
			var item_value = document.forms["myForm"][i].value; 
			
			
			if(item_value != "select"){
				
					$.ajax({
						type:'POST',
						dataType: "json",
						url:BASE_URL+"index.php?/Rent/check_availability/"+item_id+"/"+item_value+"/"+firstDate+"/"+secondDate,
						success:function(data){
							if (data['return_date']!=null)//dates are taken
							{
								var item_row = document.getElementById(item_id+'_row');
								$(item_row).css("background-color", "#ff9b9b");
								
								document.getElementById(item_id+"_glyphicon").className = "glyphicon glyphicon-exclamation-sign";
								
								items.push(data['Category']);
								return_date.push(incr_date(data['return_date']));
								sizes.push(data['item_size']);
								spot_available++;
								
							}else
							{
								total_items_added = total_items_added+1;
								document.getElementById(item_id+"_glyphicon").className = "";
								var item_row = document.getElementById(item_id+'_row');
								
								$(item_row).css("background-color", "#c7ffba");
								total_price=total_price + data['rent_price_per_day'];
								
								
								success_items.push(data['Category']);
								seccess_sizes.push(data['item_size']);
								success_item_id.push(data['id']);
								success_item_price.push(data['rent_price_per_day']);
							}
							
						},async: false // <- this turns it into synchronous
							
					});
				}else if(item_value == "select"){
					items_not_selected = items_not_selected +1;
					document.getElementById(item_id+"_glyphicon").className = "";
					var item_row = document.getElementById(item_id+'_row');
					$(item_row).css("background-color", "white");
				}
			
		}
		if(items_not_selected==4)
		{
			alert("please select a size");
		}else if(spot_available>0)
		{
				$('#myList').empty();
			var list = document.getElementById("myList");

			for (i = 1; i < items.length; i++) { 
				var node = document.createElement("LI");              
				var textnode = document.createTextNode(items[i]+" size "+sizes[i]+" is not available. Next availability is in "+return_date[i]);      
				node.appendChild(textnode);                             
				document.getElementById("myList").appendChild(node);    
			}
			
			var Item_missing_alert_modal = document.getElementById('item_missing_model');	
			Item_missing_alert_modal.style.display = "block";
			
			
	
		}else{
			$('#table_body').empty();
			var table_body = document.getElementById("table_body");
			var total_price = 0;

			for (i = 1; i < success_items.length; i++) {
				var row = table_body.insertRow(0);	
				var cell1 = row.insertCell(0);
				var cell2 = row.insertCell(1);
				var cell3 = row.insertCell(2);
				cell1.innerHTML = success_items[i];
				cell2.innerHTML = seccess_sizes[i];
				cell3.innerHTML = success_item_price[i];
				total_price = total_price + parseInt(success_item_price[i]);
			}
			diffDays= diffDays +1
			document.getElementById("total_days").innerHTML=diffDays;
			total_price = total_price *diffDays;
			document.getElementById("total_price").innerHTML=total_price+"$";
			
			var Item_missing_alert_modal = document.getElementById('summery_modal');	
			Item_missing_alert_modal.style.display = "block";
		}
		

	}
	});
});










// When the user clicks on <span> (x), close the modal
close.onclick = function() {
    summery_modal.style.display = "none";
}

modal2_close_button.onclick = function() {
	var Item_missing_alert_modal = document.getElementById('item_missing_model');
    Item_missing_alert_modal.style.display = "none";
}
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
function incr_date(date_str){
  var parts = date_str.split("-");
  var dt = new Date(
    parseInt(parts[0], 10),      // year
    parseInt(parts[1], 10) - 1,  // month (starts with 0)
    parseInt(parts[2], 10)       // date
  );
  dt.setDate(dt.getDate() + 1);
  parts[0] = "" + dt.getFullYear();
  parts[1] = "" + (dt.getMonth() + 1);
  if (parts[1].length < 2) {
    parts[1] = "0" + parts[1];
  }
  parts[2] = "" + dt.getDate();
  if (parts[2].length < 2) {
    parts[2] = "0" + parts[2];
  }
  return parts.join("-");
}



// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

        paypal.Button.render({
			


            env: 'production', // sandbox | production

            // PayPal Client IDs - replace with your own
            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
            client: {
				sandbox:    'AQ9snhtIYBC_leIy-UEUQo5TrZsTAhoWClU9VS2A_h9hflwFHv0eR2ZKowujZ8eQabgf5HWDH_vFjfEb',
                production: 'ARwYFCbYiz9zw9XhmAwSe9uCBBXZSH1pNMXtblF1TZ61q2lykXyMB-t79KnAn0FxSt-P_krCsulyiUor'
            },

            // Show the buyer a 'Pay Now' button in the checkout flow
            commit: true,

            // payment() is called when the button is clicked
            payment: function(data, actions) {

                // Make a call to the REST api to create the payment
                return actions.payment.create({
                    payment: {
                        transactions: [
                            {
                                amount: { total: '0.01', currency: 'CAD' }
                            }
                        ]
                    }
                });
            },

            // onAuthorize() is called when the buyer approves the payment
            onAuthorize: function(data, actions) {

            // Get the payment details

            return actions.payment.get().then(function(data) {

                    return actions.payment.execute().then(function() {
						
						for (i = 1; i < success_items.length; i++) {
							
							alert(success_item_id[i]);
							alert(firstDate);
							alert(secondDate);
								$.ajax({
								type:'POST',
								dataType: "json",
								url:BASE_URL+"index.php?/Rent/rent_item/"+success_item_id[i]+"/"+firstDate+"/"+secondDate,
								success:function(data){
									if(i==total_items_added){
										summery_modal.style.display = "none";
										modal_success.style.display = "block";
										$(modal_success).delay( 5000 ).fadeOut("slow");
										
										
									}
								},error:function(exception){alert("didnt work");}
								,async: false // <- this turns it into synchronous
									
								});
						}
						
						
                    });

            });
        }

        }, '#paypal-button-container');

</script>