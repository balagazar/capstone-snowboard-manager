<div id="body">
<div class="container" style="background-color:white;">
    <div class="row">
		<?php if(!$orders_empty){ ?>
       
	   	 <div class="col-sm-12 col-md-10 col-md-offset-1" >
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Shipping address</th>
						<th class="text-center">Status</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
				<?php foreach($orders as $order) { ?>
						<tr>
							<td class="col-sm-8 col-md-6">
							<div class="media">
								<a class="thumbnail pull-left" href="<?= base_url(); ?>index.php?/Products/Single_Product/<?= $order['product_id'] ?>"> <img class="media-object" src=<?= assetUrl();?><?= $order['product_picture'] ?> style="width: 72px; height: 72px;"> </a>
								<div class="media-body">
									<h4 class="media-heading"><a href="#"><?= $order['product_name'] ?></a></h4>
								</div>
							</div></td>
							<td class="col-sm-1 col-md-1 text-center"><strong><?= $order['quantity'] ?></strong></td>
							<td class="col-sm-1 col-md-3 text-center">
								<strong>
									<div><span><?= $order['shipping_address'] ?></span>, <span><?= $order['city'] ?></span></div>
									<div><span><?= $order['province'] ?></span>, <span><?= $order['postal_code'] ?></span>, <span><?= $order['country'] ?></span></div>
								</strong>
							</td>
							<td class="col-sm-1 col-md-2 text-center" id ="quantity_after_ajax"><strong><?= $order['status'] ?></strong></td>
					</tr>
				<?php } ?>
				
				
                </tbody>
            </table>
        </div>
		
		<?php }else{ ?>
		 <div class="col-sm-12 col-md-10 col-md-offset-1" >
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Prices</th>
                        <th class="text-center">Total</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
						<tr>
							<td>   </td>
							<td>   </td>
							<td>   </td>
							<td class="text-left"><h3><strong>There are no orders!</strong></h3></td>
						</tr>
				
				
                </tbody>
            </table>
        </div>
		<?php } ?>
    </div>
</div>
</div>