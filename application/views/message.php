<div id="body">
	<?php if(!$message_view){ ?>
  <div class="container-fluid" style="width:750px;">
    <div class="row" >
	
	<?php if($reported_successfully){ ?>
		<div class="alert alert-danger" id="success_message" style="z-index: 1;">
		  <strong>Success!</strong> Your report was submited succesfully!
		</div>
	<?php } ?>
	
    <div class="ui-group-buttons">
                <a  href="<?= base_url(); ?>index.php?/Message/Send_Message" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-pencil" style="padding-right:4px;"></span>Compose</a>
            </div>
        
        <div class="panel panel-default widget">
            <div class="panel-heading" >
                <span class="glyphicon glyphicon-comment"></span>
                <h3 class="panel-title">
                    Messages</h3>
            
                    
            </div>
			<?php if($read_message){ ?>
			
			<div class="container" style="margin-bottom:12.5%;margin-top:6%;">
				  <div class="row">
				<div class="col-md-6 col-md-offset-1">
					
					<div class="text-left">
					
					
					
						<br>
						<h3><strong>Subject: <?=$message['message_subject']?></strong></h3>
						<h3><strong>From: <?=$message['message_from']?></strong></h3>
						<br>
	
						
						<br/>
						<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
						<textarea name="Message_Content" style="background-color:white;height: 5em;;font-size:1.5em;" readonly class="form-control"  id="Message_Content" rows="5"><?=$message['message_content']?></textarea>
						</div>
						
						<br>
						<a  href="<?= base_url(); ?>index.php?/Message/Send_Message/<?=$message['message_from']?>" class="btn btn-sm btn-hover btn-primary" ><span class="glyphicon glyphicon-share-alt" style="padding-right:3px;"></span>Reply</a>
						<br/>
				</div>
					</div>
				</div>
				</div>
			<?php }else{ ?>
            <div class="panel-body" >
                <ul class="list-group scrollable-menu">
					<?php foreach ($listing as $row) {?>
                    <li class="list-group-item" <?php if($row['was_message_read']=='N'){?> style ="background-color:#e6f7e6"<?php } ?>>
                        <div class="row"> 
                            <div class="col-xs-2 col-md-1">
                                <img src="<?= assetUrl();?>img/<?=$row['message_from']?>/profile_pic/thumb/profile_picture_thumb.jpg" class="img-circle img-responsive" alt="" /></div>
                            <div class="col-xs-10 col-md-11">
                                    <a href="#">
                                        <?=$row['message_subject']?></a>
                                    <div class="mic-info">
                                        By: <a href="#"><?=$row['message_from']?></a> on <?=$row['message_date']?>
                                    </div>
									

								<a  href="<?= base_url(); ?>index.php?/Message/Send_Message/<?=$row['message_from']?>/<?=$row['message_subject']?>" class="btn btn-sm btn-hover btn-primary" ><span class="glyphicon glyphicon-share-alt" style="padding-right:3px;"></span>Reply</a>
								<a  href="<?= base_url(); ?>index.php?/Message/read_message/<?=$row['message_id']?>" class="btn btn-sm btn-hover btn-success" ><span class="glyphicon glyphicon-folder-open" style="padding-right:3px;"></span>View </a>
							 <div class="text-right">
									<a  href="<?= base_url(); ?>index.php?/Message/delete_message/<?=$row['message_id']?>"><span class="glyphicon glyphicon-trash" style="color:grey;font-size:1.5em;"></a>
                                    <a  href="<?= base_url(); ?>index.php?/Message/Add_report/<?=$row['message_from']?>/<?= $row['message_content']?>" ><span class="glyphicon glyphicon-alert" style="color:red;font-size:1.5em;"></a>
                                </div>
							</div>
                        </div>
                    </li>
					<?php } ?>
                </ul>
				
				 
            </div>
			<ul id="pagination-demo" class="pagination-sm"></ul>
			<?php }?>
        </div>
    </div>
</div>
  
  
  
	<?php }else { ?>
  
  <div class="container" style="">
  <div class="row">
<div class="col-md-6 col-md-offset-3">
	
	<div class="text-center">
	
	
	<?php if($UserDoesNotExist){ ?>
		<div class="alert alert-danger text-center" id="success_message" style="z-index: 1;">
		  <strong>Error!</strong> Username Does not exist!
		</div>
	<?php } ?>
		<br>
		<h2>Send Message</h2>
		<br>
		<?= form_open("Message/Send_Message_process") ?>
		 <div class="row">
					
                        <div class="col-xs-6 col-md-6">
						
	<input type="text" name="To" value="<?= $To ?>" class="form-control input-lg" placeholder="To" <?php if(isset($To)){ ?> readonly <?php } ?> required />                        
	</div>
                        <div class="col-xs-6 col-md-6">
	<input type="text" name="Subject" value="<?php if(isset($subject)){ ?><?= $subject ?><?php } ?>" class="form-control input-lg" <?php if(isset($subject)){ ?> readonly <?php } ?> placeholder="Subject"   required  />                        </div>
                    </div>
		
		<br/>
		<div class="input-group">
		<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
		<textarea name="Message_Content" class="form-control"  id="Message_Content" rows="4"></textarea>
		</div>
		<br>
		<br/>
		<?= form_submit(array('name'=>'submit', 'value'=>'Send','class'=>'btn btn-lg btn-primary btn-block','style'=>'margin-top:20px;')); ?>

		<?= form_fieldset_close(); ?>
		<?= form_close() ?>
</div>
	</div>
</div>
</div>
  <?php } ?>
</div>

