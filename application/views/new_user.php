
<div id="body" >
<br>
	
<?php if($error){?>
							<div class = "error text-center">
								<strong>
									<?=validation_errors() ?>
								</strong>
							</div>
<?php } ?>

<?php if($CreatedUser_section_1==false){ ?>

<?php if($userExists){ ?>

<div class="alert alert-danger" >
  <strong>Error!</strong> The username is already taken!
</div>

<?php } ?>
	  <div class="row">
        <div class="col-md-6 col-md-offset-3">
           			<?= form_open('New_user/Create_User_info_1',$attributes) ?>
									<legend>Sign Up</legend>
                    <div class="row">
					
                        <div class="col-xs-6 col-md-6">
                            <input type="text" name="firstname" value="" class="form-control input-lg" placeholder="First Name" required  />                        </div>
                        <div class="col-xs-6 col-md-6">
                            <input type="text" name="lastname" value="" class="form-control input-lg" placeholder="Last Name" required  />                        </div>
                    </div>
                    <input type="text" name="username" value="" class="form-control input-lg" placeholder="Username" required  />
					<input type="text" name="email" value="" class="form-control input-lg" placeholder="Your Email"  />
					<input type="password" name="password" value="" class="form-control input-lg" placeholder="Password" required  />
					<input type="password" name="ConfirmNewPass" value="" class="form-control input-lg" placeholder="Confirm Password" required  />                    <label>Birth Date</label>                    <div class="row">
						<div class="col-xs-4 col-md-4">
							<select name="month" class = "form-control input-lg" required>
								<option value="01">Jan</option>
								<option value="02">Feb</option>
								<option value="03">Mar</option>
								<option value="04">Apr</option>
								<option value="05">May</option>
								<option value="06">Jun</option>
								<option value="07">Jul</option>
								<option value="08">Aug</option>
								<option value="09">Sep</option>
								<option value="10">Oct</option>
								<option value="11">Nov</option>
								<option value="12">Dec</option>
							</select>                        
						</div>
                        <div class="col-xs-4 col-md-4">
                            <select name="day" class = "form-control input-lg" required>
							<?php 
								for ($x = 1; $x <= 31; $x++) {
								?>
									<option value="<?= $x?>"><?= $x;?></option>
								<?php
								} 
							?>
							</select>                        
						</div>
                        <div class="col-xs-4 col-md-4">
                            <select name="year" class = "form-control input-lg" >
							<?php 
								for ($x = 1920; $x <= 2013; $x++) {
								?>
									<option value="<?= $x?>"><?= $x;?></option>
								<?php
								} 
							?>
							</select>                        
						</div>
                    </div>
                     <label>Gender : </label>                    <label class="radio-inline">
                        <input type="radio" name="gender" value="M" id='male' required />                        Male
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="gender" value="F" id="female"  required />                        Female
                    </label>
                    <br />
              <span class="help-block">By clicking Create my account, you agree to our Terms and that you have read our Data Use Policy, including our Cookie Use.</span>
                    <button class="btn btn-primary btn-block" type="submit" name="submit" id="submit">
                        Create my account</button>
						
						<span id="error_message" class="text-success"></span>
						<span id="success_message" class="text-success"></span>
            </form>
			 <br /> <br />
          </div>          

	</div>
	
	
	
	<?php }else if($CreatedUser_section_1){ ?>
	   <br />
	   <br />
	<div class="row" id="security_view" >
        <div class="col-md-6 col-md-offset-3">
           			<?= form_open('New_user/Create_User_info_2',$attributes) ?>
									<legend>Add security questions</legend>
            <div class="row">

                        <label>Question 1</label>
							<select name="question_1" class = "form-control input-lg" required>
								<option value="What my dogs name?">What my dogs name?</option>
								<option value="What my childhood best friends name?">What my childhood best friends name?</option>
								<option value="What is my grandmother name?">What is my grandmother name?</option>
								<option value="What is my dream car?">What is my dream car?</option>
								<option value="What is favourite movie?">What is favourite movie?</option>
								<option value="What is favourite hobbie?">What is favourite hobbie?</option>
								<option value="What is my favourite book?">What is my favourite book?</option>
								<option value="Who is favourite celebrety?">Who is favourite celebrety?</option>
							</select>                        
						<label>Answer 1</label>
                            <input name="answer_1" type="text" class = "form-control input-lg" required></input>
							
						<label>Question 2</label>
                            <select name="question_2" class = "form-control input-lg" >
								<option value="What is my first pets name?">What is my first pets name?</option>
								<option value="What is my favourite vacation spot?">What is my favourite vacation spot?</option>
								<option value="What is my mother medan name?">What is my mother medan name?</option>
								<option value="What is my eldest cousins name?">What is my eldest cousins name?</option>
								<option value="What is my biggest fear?">What is my biggest fear?</option>
								<option value="What is the first name of the first person you kissed?">What is the first name of the first person you kissed?</option>
								<option value="What is the name of your favourite teacher?">What is the name of your favourite teacher?</option>
								<option value="In what city your closest friend lives?">In what city your closest friend lives?</option>
							</select>
							
						<label>Answer 2</label>
                            <input name="answer_2" type="text" class = "form-control input-lg" required></input>
                    <br />
              
                    <button class="btn btn-primary btn-block" type="submit" name="submit" id="submit">
                        Create my account</button>
						<br />
						<br />
						
						<span id="error_message" class="text-success"></span>
						<span id="success_message" class="text-success"></span>
            
          </div>
						<input type="hidden" name="first_name" value="<?= $first_name ?>" />
						<input type="hidden" name="last_name" value="<?= $last_name ?>" />
						<input type="hidden" name="user_name" value="<?= $user_name ?>" />
						<input type="hidden" name="email" value="<?= $email ?>" />
						<input type="hidden" name="password" value="<?= $password ?>" />
						<input type="hidden" name="ConfirmNewPass" value="<?= $ConfirmNewPass ?>" />
						<input type="hidden" name="month" value="<?= $month ?>" />
						<input type="hidden" name="day" value="<?= $day ?>" />
						<input type="hidden" name="year" value="<?= $year ?>" />
				
				</form>
   <br />

	   <br />
</div>
</div>
	<?php } ?>
	
</div>