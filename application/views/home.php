<div id="body">
<div class="container" id="homepage" >
<img  class="snowmanager_big_logo" src="<?= assetUrl();?>img/big-logo.png">
<div id="temp">loading weather data</div>
 <div class="col-xs-12 col-sm-5 col-md-12">
     <div class="">
        <div class="row">
		<h3><strong>Products</strong></h3>
		
	
			<div id="slideshow">
			   <div>
				<a href="<?= base_url(); ?>index.php?/Products/Single_Product/3">
				 <img src="<?= assetUrl();?>img/products/pic1.jpg" >
				 <div class="text"><strong>M3 Source Junior</strong></div>
				 </a>
			   </div>
			   
			   <div>
				<a href="<?= base_url(); ?>index.php?/Products/Single_Product/8">
				 <img src="<?= assetUrl();?>img/products/pic6.jpg" >
				  <div class="text"><strong>Analog gloves</strong></div>
				</a>
			   </div>
			   <div>
				<a href="<?= base_url(); ?>index.php?/Products/Single_Product/28">
				 <img src="<?= assetUrl();?>img/products/pic25.jpg" >
				  <div class="text"><strong>Smith Vantage MIPS Helmet</strong></div>
				 </a>
			   </div>
			   <div>
				<a href="<?= base_url(); ?>index.php?/Products/Single_Product/29">
				 <img src="<?= assetUrl();?>img/products/pic27.jpg" >
				   <div class="text"><strong>Sandbox Classic 2.0<br>$74.97</strong></div>
				 </a>
			   </div>
			    <div>
				<a href="<?= base_url(); ?>index.php?/Products/Single_Product/6">
				 <img src="<?= assetUrl();?>img/products/pic5.jpg" >
				 <div class="text"><strong>M3 2017 Men's Talon Wide</strong></div>
				 </a>
			   </div>
			</div>
			<br>
		</div>
	</div>


	</div>
</div>


</div>
<script>
$("#slideshow > div:gt(0)").hide();

setInterval(function() { 
  $('#slideshow > div:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('#slideshow');
},  3000);


$(document).ready(function(){
  var city = "Toronto";
  var searchtext = "select item.condition from weather.forecast where woeid in (select woeid from geo.places(1) where text='" + city + "') and u='c'"
  //change city variable dynamically as required
  $.getJSON("https://query.yahooapis.com/v1/public/yql?q=" + searchtext + "&format=json").success(function(data){
   console.log(data);
   $('#temp').html("Temperature in " + city + " is " + data.query.results.channel.item.condition.temp + "°C");
  });
});
</script>
