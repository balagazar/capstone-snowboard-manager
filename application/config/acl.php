<?php
$config['acl'] = array('home' => array('public' => true, 'member' => true, 'admin' => true),
                       'rent' => array('public' => true, 'member' => true, 'admin' => true),
                       'admin' => array('public' => false, 'member' => false, 'admin' => true),
					   'products' => array('public' => true, 'member' => false, 'admin' => true),
					   'contacts' => array('public' => true, 'member' => false, 'admin' => true),
					   'new_user' => array('public' => true, 'member' => true, 'admin' => true),
                       'login' => array('public' => true, 'member' => true, 'admin' => true),
					   'user_profile' => array('public' => false, 'member' => true, 'admin' => true)
                      );
?>